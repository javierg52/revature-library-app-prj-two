# Revature Library App PRJ-TWO

## Project Description
The Modern Library Application is a web application that allows users to login, browse and checkout books. Users can additionally add books to a wishlist and checkout multiple books from their cart. The user interface is visually appealing and easy to use.

## Technologies Used
* Spring Framework
* Angular
* Spring MVC
* Spring Web
* Bootstrap
* Google Books API 
* AWS RDB
* PostgreSQL Database Driver

## Features
* Log into the Library Application with custom email & password
* Use a text search to browse books from a catalogue backed by Google Books API 
* Add books to your wishlist or shopping cart
* Reserve books from your shopping cart to pick up from the Library
* View calendar events and contact information on the front page

### Features for Future Development
* Email newsletters for library events with reminders
* Email notifications for available books from your wishlist and/or overdue books currently reserved.

## Contributors: 
Javier Gonzalez

Andre Entrekin

Mason Underhill
