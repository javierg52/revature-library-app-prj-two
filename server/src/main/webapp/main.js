(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\mason\RevatureCode\revature-library-app-prj-two\client\src\main.ts */"zUnb");


/***/ }),

/***/ 1:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ "5aow":
/*!********************************************!*\
  !*** ./src/app/events/events.component.ts ***!
  \********************************************/
/*! exports provided: EventsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventsComponent", function() { return EventsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");


class EventsComponent {
    constructor() { }
    ngOnInit() {
    }
}
EventsComponent.ɵfac = function EventsComponent_Factory(t) { return new (t || EventsComponent)(); };
EventsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: EventsComponent, selectors: [["app-events"]], decls: 24, vars: 0, consts: [[1, "events-ctn"]], template: function EventsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "table");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Dec 18");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Dec 19");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Dec 20");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Dec 21");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Dec 22");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Beskar Appreciation Day");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Mandalorian Guild Social Night");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Foundlings Social");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Cirque de Mando");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "td");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Mandalore Folk Lore Book Club");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".events-ctn[_ngcontent-%COMP%] {\r\n  border: 1px solid #c5c4c4;\r\n  border-radius: 20px;\r\n  width: 700px;\r\n  background: white;\r\n}\r\n\r\nth[_ngcontent-%COMP%], td[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  \r\n  border-radius: 20px;\r\n  width: 150px;\r\n}\r\n\r\ntable[_ngcontent-%COMP%] {\r\n  border-radius: 20px;\r\n}\r\n\r\nth[_ngcontent-%COMP%] {\r\n  border-bottom: 1px solid #c5c4c4;\r\n  border-right: 1px solid #c5c4c4;\r\n  height: 40px;\r\n}\r\n\r\nth[_ngcontent-%COMP%]:last-child {\r\n  border-right: 0px;\r\n}\r\n\r\ntd[_ngcontent-%COMP%] {\r\n  border-right: 1px solid #c5c4c4;\r\n  height: 120px;\r\n}\r\n\r\ntd[_ngcontent-%COMP%]:last-child {\r\n  border-right: 0px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImV2ZW50cy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osaUJBQWlCO0FBQ25COztBQUVBOztFQUVFLGtCQUFrQjtFQUNsQiwrQkFBK0I7RUFDL0IsbUJBQW1CO0VBQ25CLFlBQVk7QUFDZDs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGdDQUFnQztFQUNoQywrQkFBK0I7RUFDL0IsWUFBWTtBQUNkOztBQUNBO0VBQ0UsaUJBQWlCO0FBQ25COztBQUNBO0VBQ0UsK0JBQStCO0VBQy9CLGFBQWE7QUFDZjs7QUFDQTtFQUNFLGlCQUFpQjtBQUNuQiIsImZpbGUiOiJldmVudHMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ldmVudHMtY3RuIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjYzVjNGM0O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgd2lkdGg6IDcwMHB4O1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG59XHJcblxyXG50aCxcclxudGQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAvKiBib3JkZXI6IDFweCBzb2xpZCAjYzVjNGM0OyAqL1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgd2lkdGg6IDE1MHB4O1xyXG59XHJcblxyXG50YWJsZSB7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxufVxyXG5cclxudGgge1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYzVjNGM0O1xyXG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNjNWM0YzQ7XHJcbiAgaGVpZ2h0OiA0MHB4O1xyXG59XHJcbnRoOmxhc3QtY2hpbGQge1xyXG4gIGJvcmRlci1yaWdodDogMHB4O1xyXG59XHJcbnRkIHtcclxuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjYzVjNGM0O1xyXG4gIGhlaWdodDogMTIwcHg7XHJcbn1cclxudGQ6bGFzdC1jaGlsZCB7XHJcbiAgYm9yZGVyLXJpZ2h0OiAwcHg7XHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EventsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-events',
                templateUrl: './events.component.html',
                styleUrls: ['./events.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "7/yn":
/*!************************************************!*\
  !*** ./src/app/reserved/reserved.component.ts ***!
  \************************************************/
/*! exports provided: ReservedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservedComponent", function() { return ReservedComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/api.service */ "RcPr");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");





function ReservedComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ReservedComponent_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "No books were found, check out some books! ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
} }
function ReservedComponent_ng_template_5_div_2_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ReservedComponent_ng_template_5_div_2_div_1_Template_button_click_13_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r7.returnOne($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Return");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const book_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", book_r5.book.image, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r5.book.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r5.book.author);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", book_r5.book.description, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r5.book.isbn);
} }
function ReservedComponent_ng_template_5_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ReservedComponent_ng_template_5_div_2_div_1_Template, 15, 5, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const book_r5 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", book_r5.book);
} }
function ReservedComponent_ng_template_5_Template(rf, ctx) { if (rf & 1) {
    const _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ReservedComponent_ng_template_5_Template_button_click_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11); const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r10.returnBooks($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Return All");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ReservedComponent_ng_template_5_div_2_Template, 2, 1, "div", 8);
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.books);
} }
class ReservedComponent {
    constructor(apiService, router) {
        this.apiService = apiService;
        this.router = router;
        this.isSearching = false;
        this.books = [];
        this.fname = '';
    }
    ngOnInit() {
        if (localStorage['user']) {
            this.user = JSON.parse(localStorage['user']);
        }
        else {
            this.router.navigate(['login']);
        }
        this.fname = this.user.fname;
        this.isSearching = true;
        this.apiService.getBooksForReserved(this.user.id).subscribe(res => {
            console.log(res);
            this.isSearching = false;
            this.books = res;
        }, err => {
            this.isSearching = false;
            console.log(err);
        });
    }
    returnOne(e) {
        let isbn = e.path[1].children[3].innerText;
        let index;
        for (var i = 0; i < this.books.length; i++) {
            if (this.books[i].book.isbn == isbn) {
                index = i;
            }
        }
        this.books.splice(index, 1);
        this.apiService.deleteFromReserved(isbn, this.user.id).subscribe(res => {
            console.log('Book returned');
            alert('Book returned');
        }, error => {
            console.log(error.error);
            alert(error.error);
        });
    }
    returnBooks(e) {
        this.apiService.deleteAllFromReserved(this.user.id).subscribe(res => {
            console.log('All of your books have been returned');
            alert('All of your books have been returned');
            this.books = [];
        }, error => {
            console.log(error.error);
            alert(error.error);
        });
    }
}
ReservedComponent.ɵfac = function ReservedComponent_Factory(t) { return new (t || ReservedComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"])); };
ReservedComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ReservedComponent, selectors: [["app-reserved"]], decls: 7, vars: 4, consts: [["class", "row", 4, "ngIf"], [1, "row"], [4, "ngIf", "ngIfElse"], ["elseTemplate", ""], [1, "col-12", "text-center"], ["src", "../../assets/pageTurner.gif", "alt", "Searching...", 1, "mt-4"], ["role", "alert", 1, "alert", "alert-danger"], [1, "btn", "btn-success", "basket", 3, "click"], ["class", "wrapper", 4, "ngFor", "ngForOf"], [1, "wrapper"], [4, "ngIf"], [1, ""], ["contentEditable", "", "role", "textbox", "aria-multiline", "true", 1, "card"], [3, "src"], [1, "descriptions"], [1, "card-text"], [1, "desc"], [1, "hide"], [1, "btn", "cart", 3, "click"]], template: function ReservedComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ReservedComponent_div_2_Template, 3, 0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, ReservedComponent_ng_container_4_Template, 5, 0, "ng-container", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, ReservedComponent_ng_template_5_Template, 3, 1, "ng-template", null, 3, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.fname, "'s Checked Out Books");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isSearching);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.books.length == 0)("ngIfElse", _r2);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"]], styles: ["body[_ngcontent-%COMP%] {\r\n  \r\n  padding: 0px;\r\n  margin: 0px;\r\n  width: 100%;\r\n  height: 100vh;\r\n  font-family: \"Lato\";\r\n}\r\n.row[_ngcontent-%COMP%] {\r\n  justify-content: center;\r\n}\r\n.wrapper[_ngcontent-%COMP%] {\r\n  \r\n  max-width: 300px;\r\n  width: 100%;\r\n  height: auto;\r\n  display: flex;\r\n  flex-wrap: wrap;\r\n  justify-content: center;\r\n  margin: 10px;\r\n}\r\n.card[_ngcontent-%COMP%] {\r\n  flex: 1;\r\n  flex-basis: 300px;\r\n  flex-grow: 0;\r\n  height: 440px;\r\n  width: 300px;\r\n  background: #fff;\r\n  border: 2px solid #fff;\r\n  box-shadow: 0px 4px 7px rgba(0, 0, 0, 0.5);\r\n  cursor: pointer;\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  overflow: hidden;\r\n  position: relative;\r\n}\r\n.card[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 100%;\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n}\r\n.descriptions[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  top: 0px;\r\n  left: 0px;\r\n  width: 100%;\r\n  height: 100%;\r\n  transition: all 0.7s ease-in-out;\r\n  padding: 20px;\r\n  box-sizing: border-box;\r\n  -webkit-clip-path: circle(0% at 100% 100%);\r\n          clip-path: circle(0% at 100% 100%);\r\n}\r\n.card[_ngcontent-%COMP%]:hover   .descriptions[_ngcontent-%COMP%] {\r\n  left: 0px;\r\n  transition: all 0.7s ease-in-out;\r\n  -webkit-clip-path: circle(75%);\r\n          clip-path: circle(75%);\r\n}\r\n.card[_ngcontent-%COMP%]:hover {\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.3);\r\n  transform: scale(0.97);\r\n}\r\n.card[_ngcontent-%COMP%]:hover   img[_ngcontent-%COMP%] {\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  transform: scale(1.6) rotate(20deg);\r\n  filter: blur(3px);\r\n}\r\n.card[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\r\n  color: white;\r\n  text-shadow: 0 0 5px black;\r\n  letter-spacing: 1px;\r\n  margin: 0px;\r\n  height: 68px;\r\n  overflow: hidden;\r\n}\r\n.card[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  color: white;\r\n  text-shadow: 0 0 5px black;\r\n  line-height: 24px;\r\n  height: 55%;\r\n  font-size: 20px;\r\n}\r\n.card[_ngcontent-%COMP%]:hover   .desc[_ngcontent-%COMP%] {\r\n  background: rgba(54, 54, 54, 0.356);\r\n  border-radius: 10px 0 0 10px;\r\n}\r\n.desc[_ngcontent-%COMP%] {\r\n  height: 150px;\r\n  \r\n  width: 100%;\r\n  overflow-y: scroll;\r\n  overflow-x: hidden;\r\n  padding-right: 50px;\r\n  margin-bottom: 40px;\r\n  box-sizing: content-box;\r\n  mix-blend-mode: difference;\r\n}\r\n.card[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  bottom: 15px;\r\n  margin-left: 80px;\r\n  width: 100px;\r\n  height: 40px;\r\n  cursor: pointer;\r\n  border-style: none;\r\n  background-color: #04a304;\r\n  color: #fff;\r\n  font-size: 18px;\r\n  outline: none;\r\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.4);\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.card[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover {\r\n  transform: scale(0.95) translateX(-5px);\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.hide[_ngcontent-%COMP%] {\r\n  opacity: 0;\r\n}\r\n.res-btn[_ngcontent-%COMP%] {\r\n  max-width: 200px;\r\n  display: block;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlc2VydmVkLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBK0I7RUFDL0IsWUFBWTtFQUNaLFdBQVc7RUFDWCxXQUFXO0VBQ1gsYUFBYTtFQUNiLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsdUJBQXVCO0FBQ3pCO0FBQ0E7RUFDRSxzQ0FBc0M7RUFDdEMsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtFQUNiLGVBQWU7RUFDZix1QkFBdUI7RUFDdkIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxPQUFPO0VBQ1AsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixzQkFBc0I7RUFDdEIsMENBQTBDO0VBQzFDLGVBQWU7RUFDZixxREFBcUQ7RUFDckQsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixxREFBcUQ7QUFDdkQ7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FBUztFQUNULFdBQVc7RUFDWCxZQUFZO0VBQ1osZ0NBQWdDO0VBQ2hDLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsMENBQWtDO1VBQWxDLGtDQUFrQztBQUNwQztBQUNBO0VBQ0UsU0FBUztFQUNULGdDQUFnQztFQUNoQyw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSxxREFBcUQ7RUFDckQsMENBQTBDO0VBQzFDLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UscURBQXFEO0VBQ3JELG1DQUFtQztFQUNuQyxpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0VBQ1osZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsbUNBQW1DO0VBQ25DLDRCQUE0QjtBQUM5QjtBQUNBO0VBQ0UsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QiwwQkFBMEI7QUFDNUI7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixZQUFZO0VBQ1osZUFBZTtFQUNmLGtCQUFrQjtFQUNsQix5QkFBeUI7RUFDekIsV0FBVztFQUNYLGVBQWU7RUFDZixhQUFhO0VBQ2IsMENBQTBDO0VBQzFDLGdDQUFnQztBQUNsQztBQUVBO0VBQ0UsdUNBQXVDO0VBQ3ZDLGdDQUFnQztBQUNsQztBQUVBO0VBQ0UsVUFBVTtBQUNaO0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsY0FBYztBQUNoQiIsImZpbGUiOiJyZXNlcnZlZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYm9keSB7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogIzNkM2QzZDsgKi9cclxuICBwYWRkaW5nOiAwcHg7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDB2aDtcclxuICBmb250LWZhbWlseTogXCJMYXRvXCI7XHJcbn1cclxuLnJvdyB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuLndyYXBwZXIge1xyXG4gIC8qIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpOyAqL1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIG1hcmdpbjogMTBweDtcclxufVxyXG4uY2FyZCB7XHJcbiAgZmxleDogMTtcclxuICBmbGV4LWJhc2lzOiAzMDBweDtcclxuICBmbGV4LWdyb3c6IDA7XHJcbiAgaGVpZ2h0OiA0NDBweDtcclxuICB3aWR0aDogMzAwcHg7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBib3JkZXI6IDJweCBzb2xpZCAjZmZmO1xyXG4gIGJveC1zaGFkb3c6IDBweCA0cHggN3B4IHJnYmEoMCwgMCwgMCwgMC41KTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgY3ViaWMtYmV6aWVyKDAuOCwgMC41LCAwLjIsIDEuNCk7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLmNhcmQgaW1nIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgY3ViaWMtYmV6aWVyKDAuOCwgMC41LCAwLjIsIDEuNCk7XHJcbn1cclxuLmRlc2NyaXB0aW9ucyB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMHB4O1xyXG4gIGxlZnQ6IDBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuN3MgZWFzZS1pbi1vdXQ7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIGNsaXAtcGF0aDogY2lyY2xlKDAlIGF0IDEwMCUgMTAwJSk7XHJcbn1cclxuLmNhcmQ6aG92ZXIgLmRlc2NyaXB0aW9ucyB7XHJcbiAgbGVmdDogMHB4O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjdzIGVhc2UtaW4tb3V0O1xyXG4gIGNsaXAtcGF0aDogY2lyY2xlKDc1JSk7XHJcbn1cclxuLmNhcmQ6aG92ZXIge1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGN1YmljLWJlemllcigwLjgsIDAuNSwgMC4yLCAxLjQpO1xyXG4gIGJveC1zaGFkb3c6IDBweCAycHggM3B4IHJnYmEoMCwgMCwgMCwgMC4zKTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDAuOTcpO1xyXG59XHJcbi5jYXJkOmhvdmVyIGltZyB7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgY3ViaWMtYmV6aWVyKDAuOCwgMC41LCAwLjIsIDEuNCk7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgxLjYpIHJvdGF0ZSgyMGRlZyk7XHJcbiAgZmlsdGVyOiBibHVyKDNweCk7XHJcbn1cclxuLmNhcmQgaDMge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICB0ZXh0LXNoYWRvdzogMCAwIDVweCBibGFjaztcclxuICBsZXR0ZXItc3BhY2luZzogMXB4O1xyXG4gIG1hcmdpbjogMHB4O1xyXG4gIGhlaWdodDogNjhweDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcbi5jYXJkIHAge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICB0ZXh0LXNoYWRvdzogMCAwIDVweCBibGFjaztcclxuICBsaW5lLWhlaWdodDogMjRweDtcclxuICBoZWlnaHQ6IDU1JTtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuLmNhcmQ6aG92ZXIgLmRlc2Mge1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoNTQsIDU0LCA1NCwgMC4zNTYpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHggMCAwIDEwcHg7XHJcbn1cclxuLmRlc2Mge1xyXG4gIGhlaWdodDogMTUwcHg7XHJcbiAgLyogaGVpZ2h0OiAxMDAlOyAqL1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcclxuICBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgcGFkZGluZy1yaWdodDogNTBweDtcclxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xyXG4gIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xyXG4gIG1peC1ibGVuZC1tb2RlOiBkaWZmZXJlbmNlO1xyXG59XHJcblxyXG4uY2FyZCBidXR0b24ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDE1cHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDgwcHg7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIGhlaWdodDogNDBweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgYm9yZGVyLXN0eWxlOiBub25lO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwNGEzMDQ7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgYm94LXNoYWRvdzogMHB4IDJweCAzcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG4uY2FyZCBidXR0b246aG92ZXIge1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMC45NSkgdHJhbnNsYXRlWCgtNXB4KTtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuLmhpZGUge1xyXG4gIG9wYWNpdHk6IDA7XHJcbn1cclxuXHJcbi5yZXMtYnRuIHtcclxuICBtYXgtd2lkdGg6IDIwMHB4O1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ReservedComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-reserved',
                templateUrl: './reserved.component.html',
                styleUrls: ['./reserved.component.css']
            }]
    }], function () { return [{ type: src_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }]; }, null); })();


/***/ }),

/***/ "ATD4":
/*!*****************************!*\
  !*** ./src/auth.service.ts ***!
  \*****************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");


class AuthService {
    constructor() {
        this.isLoggedIn = false;
    }
    signIn() {
        this.isLoggedIn = true;
    }
    signOut() {
        this.isLoggedIn = false;
    }
}
AuthService.ɵfac = function AuthService_Factory(t) { return new (t || AuthService)(); };
AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: AuthService, factory: AuthService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "Oh3b":
/*!************************************************!*\
  !*** ./src/app/homepage/homepage.component.ts ***!
  \************************************************/
/*! exports provided: HomepageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageComponent", function() { return HomepageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _books_books_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../books/books.component */ "PRiU");
/* harmony import */ var _staff_faves_staff_faves_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../staff-faves/staff-faves.component */ "iBUs");
/* harmony import */ var _events_events_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../events/events.component */ "5aow");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../contact/contact.component */ "bzTf");






class HomepageComponent {
    constructor() { }
    ngOnInit() {
    }
}
HomepageComponent.ɵfac = function HomepageComponent_Factory(t) { return new (t || HomepageComponent)(); };
HomepageComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomepageComponent, selectors: [["app-homepage"]], decls: 15, vars: 0, consts: [["rel", "preconnect", "href", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtrustConstantResourceUrl"]("https://fonts.gstatic.com")], ["href", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtrustConstantResourceUrl"]("https://fonts.googleapis.com/css2?family=Lobster&display=swap"), "rel", "stylesheet"], [1, "container", "welcome"], [1, "container"], [1, "row", "top"], [1, "col-4", "staff"], [1, "row", "col-7"], [1, "events"], [1, "row", "col-8"], [1, "contact"]], template: function HomepageComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "link", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "link", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Welcome to the Public Library of Mandalore ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "app-books");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-staff-faves");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "app-events");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "app-contact");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_books_books_component__WEBPACK_IMPORTED_MODULE_1__["BooksComponent"], _staff_faves_staff_faves_component__WEBPACK_IMPORTED_MODULE_2__["StaffFavesComponent"], _events_events_component__WEBPACK_IMPORTED_MODULE_3__["EventsComponent"], _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__["ContactComponent"]], styles: [".welcome[_ngcontent-%COMP%] {\r\n  font-family: \"Lobster\", cursive;\r\n  font-size: 40px;\r\n  color: #0e7a0e;\r\n}\r\n\r\n.top[_ngcontent-%COMP%] {\r\n  margin-top: -17px;\r\n}\r\n\r\n.staff[_ngcontent-%COMP%] {\r\n  margin-left: 90px;\r\n}\r\n\r\n.events[_ngcontent-%COMP%] {\r\n  height: 130px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWVwYWdlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBK0I7RUFDL0IsZUFBZTtFQUNmLGNBQWM7QUFDaEI7O0FBRUE7RUFDRSxpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxhQUFhO0FBQ2YiLCJmaWxlIjoiaG9tZXBhZ2UuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi53ZWxjb21lIHtcclxuICBmb250LWZhbWlseTogXCJMb2JzdGVyXCIsIGN1cnNpdmU7XHJcbiAgZm9udC1zaXplOiA0MHB4O1xyXG4gIGNvbG9yOiAjMGU3YTBlO1xyXG59XHJcblxyXG4udG9wIHtcclxuICBtYXJnaW4tdG9wOiAtMTdweDtcclxufVxyXG5cclxuLnN0YWZmIHtcclxuICBtYXJnaW4tbGVmdDogOTBweDtcclxufVxyXG5cclxuLmV2ZW50cyB7XHJcbiAgaGVpZ2h0OiAxMzBweDtcclxufVxyXG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomepageComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-homepage',
                templateUrl: './homepage.component.html',
                styleUrls: ['./homepage.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "Oj1a":
/*!****************************!*\
  !*** ./src/models/user.ts ***!
  \****************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
class User {
    constructor(id, fname, lname, username, email, phone, memSince) {
        this.memSince = "today";
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.memSince = memSince;
    }
}


/***/ }),

/***/ "PRiU":
/*!******************************************!*\
  !*** ./src/app/books/books.component.ts ***!
  \******************************************/
/*! exports provided: BooksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BooksComponent", function() { return BooksComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../api.service */ "RcPr");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "ofXK");









const _c0 = ["bookSearchInput"];
function BooksComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Book added to Cart!");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function BooksComponent_ng_container_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Book added to Wishlist!");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
} }
function BooksComponent_div_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function BooksComponent_ng_container_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "No books were found, please try again");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
} }
function BooksComponent_ng_template_11_div_0_div_1_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const book_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", book_r8.volumeInfo.industryIdentifiers[0].identifier, "");
} }
function BooksComponent_ng_template_11_div_0_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, BooksComponent_ng_template_11_div_0_div_1_div_11_Template, 2, 1, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "button", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BooksComponent_ng_template_11_div_0_div_1_Template_button_click_12_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r12.addToCart($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Cart");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BooksComponent_ng_template_11_div_0_div_1_Template_button_click_14_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13); const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r14.addToWishlist($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Wishlist");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const book_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", book_r8.volumeInfo.imageLinks.thumbnail, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r8.volumeInfo.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r8.volumeInfo.authors);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", book_r8.volumeInfo.description, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", book_r8.volumeInfo.industryIdentifiers !== undefined);
} }
function BooksComponent_ng_template_11_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, BooksComponent_ng_template_11_div_0_div_1_Template, 16, 5, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const book_r8 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", book_r8.volumeInfo);
} }
function BooksComponent_ng_template_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, BooksComponent_ng_template_11_div_0_Template, 2, 1, "div", 14);
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r6.apiResponse["items"]);
} }
const PARAMS = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]({
    fromObject: {
        action: "opensearch",
        format: "json",
        origin: "*"
    }
});
class BooksComponent {
    constructor(http, apiService, router) {
        this.http = http;
        this.apiService = apiService;
        this.router = router;
        this.isSearching = false;
        this.addedToCart = false;
        this.addedToWishlist = false;
        this.apiResponse = [];
        this.cartName = 'Cart';
        this.wishName = 'WishList';
        if (localStorage['user']) {
            this.user = JSON.parse(localStorage['user']);
        }
    }
    ngOnInit() {
        console.log(JSON.parse(localStorage['user']));
        console.log(this.user.id);
        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["fromEvent"])(this.bookSearchInput.nativeElement, 'keyup').pipe(
        // get value
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])((event) => {
            return event.target.value;
        })
        // if character length greater then 2
        , Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["filter"])(res => res.length > 2)
        // Time in milliseconds between key events
        , Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["debounceTime"])(500)
        // If previous query is diffent from current   
        , Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["distinctUntilChanged"])()
        // subscription for response
        ).subscribe((text) => {
            this.isSearching = true;
            this.apiService.getBooks(text).subscribe((res) => {
                console.log('res', res);
                this.isSearching = false;
                this.apiResponse = res;
            }, (err) => {
                this.isSearching = false;
                console.log('error', err);
            });
        });
    }
    addToCart(e) {
        this.cartName = "Added!";
        console.log(e);
        if (!localStorage.getItem('user')) {
            this.router.navigate(['login']);
        }
        let image = e.path[2].children[0].currentSrc;
        let isbn = e.path[1].children[3].innerText;
        let author = e.path[2].childNodes[1].childNodes[1].innerText;
        let title = e.path[2].childNodes[1].childNodes[0].innerText;
        let desc = e.path[1].childNodes[2].innerText;
        let book = { "title": title, "author": author, "image": image, "isbn": isbn, "description": desc };
        console.log(book);
        console.log("id", this.user.id);
        this.apiService.addBookToCart(this.user.id, book).subscribe(res => {
            console.log('Book added to cart');
            alert('Book added to cart');
            setTimeout(() => {
                this.addedToCart = true;
            }, 2000);
        }, error => {
            console.log(error.error);
            alert(error.error);
        });
    }
    ;
    addToWishlist(e) {
        this.addedToWishlist = true;
        this.wishName = "Added!";
        console.log(e);
        if (!localStorage.getItem('user')) {
            this.router.navigate(['login']);
        }
        let image = e.path[2].children[0].currentSrc;
        let isbn = e.path[1].children[3].innerText;
        let author = e.path[2].childNodes[1].childNodes[1].innerText;
        let title = e.path[2].childNodes[1].childNodes[0].innerText;
        let desc = e.path[1].childNodes[2].innerText;
        let book = { "title": title, "author": author, "image": image, "isbn": isbn, "description": desc };
        console.log(book);
        this.apiService.addBookToWishlist(this.user.id, book).subscribe(res => {
            console.log('Book added to wishlist');
            alert('Book added to wishlist');
            setTimeout(() => {
                this.addedToWishlist = true;
            }, 2000);
        }, error => {
            console.log(error.error);
            alert(error.error);
        });
    }
}
BooksComponent.ɵfac = function BooksComponent_Factory(t) { return new (t || BooksComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"])); };
BooksComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BooksComponent, selectors: [["app-books"]], viewQuery: function BooksComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstaticViewQuery"](_c0, true);
    } if (rf & 2) {
        let _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.bookSearchInput = _t.first);
    } }, decls: 13, vars: 5, consts: [[2, "margin", "50px"], [1, "container", 2, "text-align", "center"], [1, "row"], [1, "col-12", "text-center"], [4, "ngIf"], ["type", "text", "placeholder", "Type any book or author name", 1, "form-control", "search-bar", "mb-4"], ["bookSearchInput", ""], ["class", "row", 4, "ngIf"], [4, "ngIf", "ngIfElse"], ["elseTemplate", ""], [1, "col-8", "text-center", "widget"], ["role", "alert", 1, "alert", "alert-success"], ["src", "../../assets/pageTurner.gif", "alt", "Searching...", 1, "mt-4"], ["role", "alert", 1, "alert", "alert-danger"], ["class", "wrapper", 4, "ngFor", "ngForOf"], [1, "wrapper"], [1, ""], ["contentEditable", "", "role", "textbox", "aria-multiline", "true", 1, "card"], [3, "src"], [1, "descriptions"], [1, "card-text"], [1, "desc"], ["class", "hide", 4, "ngIf"], [1, "btn", "cart", 3, "click"], [1, "btn", "wish", 3, "click"], [1, "hide"]], template: function BooksComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, BooksComponent_div_4_Template, 5, 0, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, BooksComponent_ng_container_5_Template, 5, 0, "ng-container", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 5, 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, BooksComponent_div_8_Template, 3, 0, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, BooksComponent_ng_container_10_Template, 5, 0, "ng-container", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, BooksComponent_ng_template_11_Template, 1, 1, "ng-template", null, 9, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.addedToCart);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.addedToWishlist);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isSearching);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.apiResponse.totalItems == 0)("ngIfElse", _r5);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"]], styles: ["body[_ngcontent-%COMP%] {\r\n  \r\n  padding: 0px;\r\n  margin: 0px;\r\n  width: 100%;\r\n  height: 100vh;\r\n  font-family: \"Lato\";\r\n}\r\n.row[_ngcontent-%COMP%] {\r\n  justify-content: center;\r\n}\r\n.wrapper[_ngcontent-%COMP%] {\r\n  \r\n  max-width: 300px;\r\n  width: 100%;\r\n  height: auto;\r\n  display: flex;\r\n  flex-wrap: wrap;\r\n  justify-content: center;\r\n  margin: 10px;\r\n}\r\n.card[_ngcontent-%COMP%] {\r\n  flex: 1;\r\n  flex-basis: 300px;\r\n  flex-grow: 0;\r\n  height: 440px;\r\n  width: 300px;\r\n  background: #fff;\r\n  border: 2px solid #fff;\r\n  box-shadow: 0px 4px 7px rgba(0, 0, 0, 0.5);\r\n  cursor: pointer;\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  overflow: hidden;\r\n  position: relative;\r\n}\r\n.card[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 100%;\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n}\r\n.descriptions[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  top: 0px;\r\n  left: 0px;\r\n  width: 100%;\r\n  height: 100%;\r\n  transition: all 0.7s ease-in-out;\r\n  padding: 20px;\r\n  box-sizing: border-box;\r\n  -webkit-clip-path: circle(0% at 100% 100%);\r\n          clip-path: circle(0% at 100% 100%);\r\n}\r\n.card[_ngcontent-%COMP%]:hover   .descriptions[_ngcontent-%COMP%] {\r\n  left: 0px;\r\n  transition: all 0.7s ease-in-out;\r\n  -webkit-clip-path: circle(75%);\r\n          clip-path: circle(75%);\r\n}\r\n.card[_ngcontent-%COMP%]:hover {\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.3);\r\n  transform: scale(0.97);\r\n}\r\n.card[_ngcontent-%COMP%]:hover   img[_ngcontent-%COMP%] {\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  transform: scale(1.6) rotate(20deg);\r\n  filter: blur(3px);\r\n}\r\n.card[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\r\n  color: white;\r\n  text-shadow: 0 0 5px black;\r\n  letter-spacing: 1px;\r\n  margin: 0px;\r\n  height: 68px;\r\n  overflow: hidden;\r\n}\r\n.card[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  color: white;\r\n  text-shadow: 0 0 5px black;\r\n  line-height: 24px;\r\n  height: 55%;\r\n  font-size: 20px;\r\n}\r\n.card[_ngcontent-%COMP%]:hover   .desc[_ngcontent-%COMP%] {\r\n  background: rgba(54, 54, 54, 0.356);\r\n  border-radius: 10px 0 0 10px;\r\n}\r\n.desc[_ngcontent-%COMP%] {\r\n  height: 150px;\r\n  \r\n  width: 100%;\r\n  overflow-y: scroll;\r\n  overflow-x: hidden;\r\n  padding-right: 50px;\r\n  margin-bottom: 40px;\r\n  box-sizing: content-box;\r\n  mix-blend-mode: difference;\r\n}\r\n.card[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  bottom: 15px;\r\n  width: 100px;\r\n  height: 40px;\r\n  cursor: pointer;\r\n  border-style: none;\r\n  background-color: #04a304;\r\n  color: #fff;\r\n  font-size: 18px;\r\n  outline: none;\r\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.4);\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.wish[_ngcontent-%COMP%] {\r\n  right: 30px;\r\n}\r\n.cart[_ngcontent-%COMP%] {\r\n  left: 30px;\r\n}\r\n.card[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover {\r\n  transform: scale(0.95) translateX(-5px);\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.hide[_ngcontent-%COMP%] {\r\n  opacity: 0;\r\n}\r\n.widget[_ngcontent-%COMP%] {\r\n  position: fixed;\r\n  margin: auto;\r\n  top: 0%;\r\n  opacity: 0;\r\n  animation: fadein 2s;\r\n  -webkit-animation: fadein 2s;\r\n  -moz-animation: fadein 2s;\r\n  \r\n}\r\n@keyframes fadein {\r\n  from {\r\n    opacity: 0;\r\n  }\r\n  to {\r\n    opacity: 1;\r\n  }\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJvb2tzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBK0I7RUFDL0IsWUFBWTtFQUNaLFdBQVc7RUFDWCxXQUFXO0VBQ1gsYUFBYTtFQUNiLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsdUJBQXVCO0FBQ3pCO0FBQ0E7RUFDRSxzQ0FBc0M7RUFDdEMsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtFQUNiLGVBQWU7RUFDZix1QkFBdUI7RUFDdkIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxPQUFPO0VBQ1AsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixzQkFBc0I7RUFDdEIsMENBQTBDO0VBQzFDLGVBQWU7RUFDZixxREFBcUQ7RUFDckQsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixxREFBcUQ7QUFDdkQ7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FBUztFQUNULFdBQVc7RUFDWCxZQUFZO0VBQ1osZ0NBQWdDO0VBQ2hDLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsMENBQWtDO1VBQWxDLGtDQUFrQztBQUNwQztBQUNBO0VBQ0UsU0FBUztFQUNULGdDQUFnQztFQUNoQyw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSxxREFBcUQ7RUFDckQsMENBQTBDO0VBQzFDLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UscURBQXFEO0VBQ3JELG1DQUFtQztFQUNuQyxpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0VBQ1osZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsbUNBQW1DO0VBQ25DLDRCQUE0QjtBQUM5QjtBQUNBO0VBQ0UsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QiwwQkFBMEI7QUFDNUI7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osWUFBWTtFQUNaLFlBQVk7RUFDWixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixXQUFXO0VBQ1gsZUFBZTtFQUNmLGFBQWE7RUFDYiwwQ0FBMEM7RUFDMUMsZ0NBQWdDO0FBQ2xDO0FBQ0E7RUFDRSxXQUFXO0FBQ2I7QUFDQTtFQUNFLFVBQVU7QUFDWjtBQUVBO0VBQ0UsdUNBQXVDO0VBQ3ZDLGdDQUFnQztBQUNsQztBQUVBO0VBQ0UsVUFBVTtBQUNaO0FBRUE7RUFDRSxlQUFlO0VBQ2YsWUFBWTtFQUNaLE9BQU87RUFDUCxVQUFVO0VBQ1Ysb0JBQW9CO0VBQ3BCLDRCQUE0QjtFQUM1Qix5QkFBeUI7RUFDekIsNkJBQTZCO0FBQy9CO0FBRUE7RUFDRTtJQUNFLFVBQVU7RUFDWjtFQUNBO0lBQ0UsVUFBVTtFQUNaO0FBQ0YiLCJmaWxlIjoiYm9va3MuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImJvZHkge1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6ICMzZDNkM2Q7ICovXHJcbiAgcGFkZGluZzogMHB4O1xyXG4gIG1hcmdpbjogMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwdmg7XHJcbiAgZm9udC1mYW1pbHk6IFwiTGF0b1wiO1xyXG59XHJcbi5yb3cge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcbi53cmFwcGVyIHtcclxuICAvKiB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTsgKi9cclxuICBtYXgtd2lkdGg6IDMwMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogYXV0bztcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtd3JhcDogd3JhcDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBtYXJnaW46IDEwcHg7XHJcbn1cclxuLmNhcmQge1xyXG4gIGZsZXg6IDE7XHJcbiAgZmxleC1iYXNpczogMzAwcHg7XHJcbiAgZmxleC1ncm93OiAwO1xyXG4gIGhlaWdodDogNDQwcHg7XHJcbiAgd2lkdGg6IDMwMHB4O1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgYm9yZGVyOiAycHggc29saWQgI2ZmZjtcclxuICBib3gtc2hhZG93OiAwcHggNHB4IDdweCByZ2JhKDAsIDAsIDAsIDAuNSk7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGN1YmljLWJlemllcigwLjgsIDAuNSwgMC4yLCAxLjQpO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5jYXJkIGltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGN1YmljLWJlemllcigwLjgsIDAuNSwgMC4yLCAxLjQpO1xyXG59XHJcbi5kZXNjcmlwdGlvbnMge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDBweDtcclxuICBsZWZ0OiAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjdzIGVhc2UtaW4tb3V0O1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBjbGlwLXBhdGg6IGNpcmNsZSgwJSBhdCAxMDAlIDEwMCUpO1xyXG59XHJcbi5jYXJkOmhvdmVyIC5kZXNjcmlwdGlvbnMge1xyXG4gIGxlZnQ6IDBweDtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC43cyBlYXNlLWluLW91dDtcclxuICBjbGlwLXBhdGg6IGNpcmNsZSg3NSUpO1xyXG59XHJcbi5jYXJkOmhvdmVyIHtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBjdWJpYy1iZXppZXIoMC44LCAwLjUsIDAuMiwgMS40KTtcclxuICBib3gtc2hhZG93OiAwcHggMnB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMyk7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgwLjk3KTtcclxufVxyXG4uY2FyZDpob3ZlciBpbWcge1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGN1YmljLWJlemllcigwLjgsIDAuNSwgMC4yLCAxLjQpO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMS42KSByb3RhdGUoMjBkZWcpO1xyXG4gIGZpbHRlcjogYmx1cigzcHgpO1xyXG59XHJcbi5jYXJkIGgzIHtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgdGV4dC1zaGFkb3c6IDAgMCA1cHggYmxhY2s7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcclxuICBtYXJnaW46IDBweDtcclxuICBoZWlnaHQ6IDY4cHg7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4uY2FyZCBwIHtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgdGV4dC1zaGFkb3c6IDAgMCA1cHggYmxhY2s7XHJcbiAgbGluZS1oZWlnaHQ6IDI0cHg7XHJcbiAgaGVpZ2h0OiA1NSU7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG59XHJcbi5jYXJkOmhvdmVyIC5kZXNjIHtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDU0LCA1NCwgNTQsIDAuMzU2KTtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4IDAgMCAxMHB4O1xyXG59XHJcbi5kZXNjIHtcclxuICBoZWlnaHQ6IDE1MHB4O1xyXG4gIC8qIGhlaWdodDogMTAwJTsgKi9cclxuICB3aWR0aDogMTAwJTtcclxuICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xyXG4gIHBhZGRpbmctcmlnaHQ6IDUwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcclxuICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcclxuICBtaXgtYmxlbmQtbW9kZTogZGlmZmVyZW5jZTtcclxufVxyXG5cclxuLmNhcmQgYnV0dG9uIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgYm90dG9tOiAxNXB4O1xyXG4gIHdpZHRoOiAxMDBweDtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGJvcmRlci1zdHlsZTogbm9uZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDRhMzA0O1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBvdXRsaW5lOiBub25lO1xyXG4gIGJveC1zaGFkb3c6IDBweCAycHggM3B4IHJnYmEoMCwgMCwgMCwgMC40KTtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlLWluLW91dDtcclxufVxyXG4ud2lzaCB7XHJcbiAgcmlnaHQ6IDMwcHg7XHJcbn1cclxuLmNhcnQge1xyXG4gIGxlZnQ6IDMwcHg7XHJcbn1cclxuXHJcbi5jYXJkIGJ1dHRvbjpob3ZlciB7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgwLjk1KSB0cmFuc2xhdGVYKC01cHgpO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG4uaGlkZSB7XHJcbiAgb3BhY2l0eTogMDtcclxufVxyXG5cclxuLndpZGdldCB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICB0b3A6IDAlO1xyXG4gIG9wYWNpdHk6IDA7XHJcbiAgYW5pbWF0aW9uOiBmYWRlaW4gMnM7XHJcbiAgLXdlYmtpdC1hbmltYXRpb246IGZhZGVpbiAycztcclxuICAtbW96LWFuaW1hdGlvbjogZmFkZWluIDJzO1xyXG4gIC8qIHRyYW5zaXRpb246IDFzIGFsbCBlYXNlOyAqL1xyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGZhZGVpbiB7XHJcbiAgZnJvbSB7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gIH1cclxuICB0byB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxufVxyXG5cclxuQC1tb3ota2V5ZnJhbWVzIGZhZGVpbiB7XHJcbiAgZnJvbSB7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gIH1cclxuICB0byB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxufVxyXG5cclxuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGVpbiB7XHJcbiAgZnJvbSB7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gIH1cclxuICB0byB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxufVxyXG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BooksComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-books',
                templateUrl: './books.component.html',
                styleUrls: ['./books.component.css']
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }, { type: _api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }]; }, { bookSearchInput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['bookSearchInput', { static: true }]
        }] }); })();


/***/ }),

/***/ "RcPr":
/*!****************************!*\
  !*** ./src/api.service.ts ***!
  \****************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");




class ApiService {
    constructor(http) {
        this.http = http;
    }
    getBooks(term) {
        if (term === '') {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])([]);
        }
        return this.http.get('https://www.googleapis.com/books/v1/volumes?q=' + term + '&maxResults=40');
    }
    //done
    getUser(username) {
        return this.http.get(`http://localhost:8080/api/get-user?username=${username}`);
    }
    //done
    signUp(form) {
        return this.http.post('http://localhost:8080/api/signup', form);
    }
    //done
    editProfile(user, userId) {
        return this.http.put(`http://localhost:8080/api/update-profile?userId=${userId}`, user);
    }
    //done
    addBookToCart(userId, book) {
        return this.http.post(`http://localhost:8080/api/cart/add-cart?userId=${userId}`, book, { responseType: 'text' });
    }
    //done
    getBooksForCart(userId) {
        return this.http.get(`http://localhost:8080/api/cart/get-cart?userId=${userId}`);
    }
    //done
    deleteFromCart(isbn, userId) {
        return this.http.delete(`http://localhost:8080/api/cart/delete-from-cart?isbn=${isbn}&userId=${userId}`, { responseType: 'text' });
    }
    //done
    deleteAllFromCart(userId) {
        return this.http.delete(`http://localhost:8080/api/cart/delete-all-from-cart?userId=${userId}`, { responseType: 'text' });
    }
    //done
    addBookAllReserved(userId, book) {
        return this.http.post(`http://localhost:8080/api/reserved/add-all-reserved?userId=${userId}`, book, { responseType: 'text' });
    }
    //done
    getBooksForReserved(userId) {
        return this.http.get(`http://localhost:8080/api/reserved/get-reserved?userId=${userId}`);
    }
    //done
    deleteFromReserved(isbn, userId) {
        return this.http.delete(`http://localhost:8080/api/reserved/delete-from-reserved?isbn=${isbn}&userId=${userId}`, { responseType: 'text' });
    }
    //done
    deleteAllFromReserved(userId) {
        return this.http.delete(`http://localhost:8080/api/reserved/delete-all-from-reserved?userId=${userId}`, { responseType: 'text' });
    }
    //done
    addBookToWishlist(userId, book) {
        return this.http.post(`http://localhost:8080/api/wishlist/add-wish-list?userId=${userId}`, book, { responseType: 'text' });
    }
    //done
    getBooksForWishlist(userId) {
        return this.http.get(`http://localhost:8080/api/wishlist/get-wishlist?userId=${userId}`);
    }
    //done
    deleteFromWishlist(isbn, userId) {
        return this.http.delete(`http://localhost:8080/api/wishlist/delete-from-wishlist?isbn=${isbn}&userId=${userId}`, { responseType: 'text' });
    }
    //done
    deleteAllFromWishlist(userId) {
        return this.http.delete(`http://localhost:8080/api/wishlist/delete-all-from-wishlist?userId=${userId}`, { responseType: 'text' });
    }
}
ApiService.ɵfac = function ApiService_Factory(t) { return new (t || ApiService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
ApiService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ApiService, factory: ApiService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ApiService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./navbar/navbar.component */ "kWWo");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");




class AppComponent {
    constructor() {
        this.title = 'ui';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 3, vars: 0, consts: [[1, "background"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__["NavbarComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"]], styles: [".background[_ngcontent-%COMP%] {\r\n  background: url('au-bg-blur.png');\r\n  position: static;\r\n  width: 100%;\r\n  min-height: 100vh;\r\n  background-repeat: no-repeat;\r\n  background-position: center;\r\n  background-size: cover;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUNBQTJDO0VBQzNDLGdCQUFnQjtFQUNoQixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLDRCQUE0QjtFQUM1QiwyQkFBMkI7RUFDM0Isc0JBQXNCO0FBQ3hCIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJhY2tncm91bmQge1xyXG4gIGJhY2tncm91bmQ6IHVybChcIi4uL2Fzc2V0cy9hdS1iZy1ibHVyLnBuZ1wiKTtcclxuICBwb3NpdGlvbjogc3RhdGljO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG1pbi1oZWlnaHQ6IDEwMHZoO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], null, null); })();


/***/ }),

/***/ "W6KJ":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");



class ProfileComponent {
    constructor() {
        this.fname = '';
        this.lname = '';
        this.username = '';
        this.email = '';
        this.phone = '';
        if (localStorage['user']) {
            this.user = JSON.parse(localStorage['user']);
        }
    }
    ngOnInit() {
        console.log(this.user);
        this.fname = this.user.fname;
        this.lname = this.user.lname;
        this.username = this.user.username;
        this.email = this.user.email;
        this.phone = this.user.phone;
        this.memSince = this.user.memSince;
    }
}
ProfileComponent.ɵfac = function ProfileComponent_Factory(t) { return new (t || ProfileComponent)(); };
ProfileComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ProfileComponent, selectors: [["app-profile"]], decls: 28, vars: 6, consts: [["id", "profile", 1, "container", "center"], [1, "left"], ["routerLink", "/edit-profile"], [1, "fas", "fa-cog"], [1, "fname"], [1, "lname"], [1, "username"], [1, "email"], [1, "sectionhone"], [1, "member-since"]], template: function ProfileComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "section", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "First Name:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "section", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Last Name:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "section", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Username:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "section", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Email:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "section", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Phone:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "section", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Member Since:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.fname, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.lname, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.username, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" \u00A0 \u00A0\u00A0\u00A0\u00A0\u00A0 ", ctx.email, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" \u00A0\u00A0\u00A0\u00A0\u00A0\u00A0", ctx.phone, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.memSince, "");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]], styles: ["#profile[_ngcontent-%COMP%] {\r\n  border: 1px solid black;\r\n  padding: 30px -30px;\r\n  width: 300px;\r\n  border-radius: 20px;\r\n}\r\n\r\nsection[_ngcontent-%COMP%] {\r\n  margin: 10px 0;\r\n}\r\n\r\n.fa-cog[_ngcontent-%COMP%] {\r\n  float: right;\r\n  padding: 5px 5px 0 0;\r\n  font-size: 25px;\r\n}\r\n\r\na[_ngcontent-%COMP%], span[_ngcontent-%COMP%] {\r\n  color: #04a304;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2ZpbGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGNBQWM7QUFDaEI7O0FBQ0E7RUFDRSxZQUFZO0VBQ1osb0JBQW9CO0VBQ3BCLGVBQWU7QUFDakI7O0FBQ0E7O0VBRUUsY0FBYztBQUNoQiIsImZpbGUiOiJwcm9maWxlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjcHJvZmlsZSB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XHJcbiAgcGFkZGluZzogMzBweCAtMzBweDtcclxuICB3aWR0aDogMzAwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxufVxyXG5cclxuc2VjdGlvbiB7XHJcbiAgbWFyZ2luOiAxMHB4IDA7XHJcbn1cclxuLmZhLWNvZyB7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG4gIHBhZGRpbmc6IDVweCA1cHggMCAwO1xyXG4gIGZvbnQtc2l6ZTogMjVweDtcclxufVxyXG5hLFxyXG5zcGFuIHtcclxuICBjb2xvcjogIzA0YTMwNDtcclxufVxyXG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProfileComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-profile',
                templateUrl: './profile.component.html',
                styleUrls: ['./profile.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../api.service */ "RcPr");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./navbar/navbar.component */ "kWWo");
/* harmony import */ var _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./homepage/homepage.component */ "Oh3b");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./profile/profile.component */ "W6KJ");
/* harmony import */ var _events_events_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./events/events.component */ "5aow");
/* harmony import */ var _cart_cart_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./cart/cart.component */ "c2A7");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./signup/signup.component */ "rd1V");
/* harmony import */ var _editprofile_editprofile_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./editprofile/editprofile.component */ "aEgf");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./login/login.component */ "vtpD");
/* harmony import */ var _books_books_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./books/books.component */ "PRiU");
/* harmony import */ var _reserved_reserved_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./reserved/reserved.component */ "7/yn");
/* harmony import */ var _wishlist_wishlist_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./wishlist/wishlist.component */ "x7zu");
/* harmony import */ var _logout_logout_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./logout/logout.component */ "n1B2");
/* harmony import */ var _staff_faves_staff_faves_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./staff-faves/staff-faves.component */ "iBUs");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./contact/contact.component */ "bzTf");























class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"]], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
        _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__["NavbarComponent"],
        _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_9__["HomepageComponent"],
        _profile_profile_component__WEBPACK_IMPORTED_MODULE_10__["ProfileComponent"],
        _events_events_component__WEBPACK_IMPORTED_MODULE_11__["EventsComponent"],
        _cart_cart_component__WEBPACK_IMPORTED_MODULE_12__["CartComponent"],
        _signup_signup_component__WEBPACK_IMPORTED_MODULE_13__["SignupComponent"],
        _editprofile_editprofile_component__WEBPACK_IMPORTED_MODULE_14__["EditprofileComponent"],
        _login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"],
        _books_books_component__WEBPACK_IMPORTED_MODULE_16__["BooksComponent"],
        _reserved_reserved_component__WEBPACK_IMPORTED_MODULE_17__["ReservedComponent"],
        _wishlist_wishlist_component__WEBPACK_IMPORTED_MODULE_18__["WishlistComponent"],
        _logout_logout_component__WEBPACK_IMPORTED_MODULE_19__["LogoutComponent"],
        _staff_faves_staff_faves_component__WEBPACK_IMPORTED_MODULE_20__["StaffFavesComponent"],
        _contact_contact_component__WEBPACK_IMPORTED_MODULE_21__["ContactComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
        _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                    _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__["NavbarComponent"],
                    _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_9__["HomepageComponent"],
                    _profile_profile_component__WEBPACK_IMPORTED_MODULE_10__["ProfileComponent"],
                    _events_events_component__WEBPACK_IMPORTED_MODULE_11__["EventsComponent"],
                    _cart_cart_component__WEBPACK_IMPORTED_MODULE_12__["CartComponent"],
                    _signup_signup_component__WEBPACK_IMPORTED_MODULE_13__["SignupComponent"],
                    _editprofile_editprofile_component__WEBPACK_IMPORTED_MODULE_14__["EditprofileComponent"],
                    _login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"],
                    _books_books_component__WEBPACK_IMPORTED_MODULE_16__["BooksComponent"],
                    _reserved_reserved_component__WEBPACK_IMPORTED_MODULE_17__["ReservedComponent"],
                    _wishlist_wishlist_component__WEBPACK_IMPORTED_MODULE_18__["WishlistComponent"],
                    _logout_logout_component__WEBPACK_IMPORTED_MODULE_19__["LogoutComponent"],
                    _staff_faves_staff_faves_component__WEBPACK_IMPORTED_MODULE_20__["StaffFavesComponent"],
                    _contact_contact_component__WEBPACK_IMPORTED_MODULE_21__["ContactComponent"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
                ],
                providers: [_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"]],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "aEgf":
/*!******************************************************!*\
  !*** ./src/app/editprofile/editprofile.component.ts ***!
  \******************************************************/
/*! exports provided: EditprofileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditprofileComponent", function() { return EditprofileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_models_user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/models/user */ "Oj1a");
/* harmony import */ var bcryptjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! bcryptjs */ "J5zx");
/* harmony import */ var bcryptjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(bcryptjs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/api.service */ "RcPr");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");







class EditprofileComponent {
    constructor(apiService, formBuilder, router) {
        this.apiService = apiService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.userId = 0;
        this.salt = '';
        if (localStorage['user']) {
            this.user = JSON.parse(localStorage['user']);
        }
        this.editProfile = this.formBuilder.group({
            firstName: this.user.fname,
            lastName: this.user.lname,
            username: this.user.username,
            passHash: '',
            email: this.user.email,
            phone: this.user.phone,
        });
    }
    ngOnInit() {
        this.user = JSON.parse(localStorage['user']);
        console.log(this.user);
        this.userId = this.user.id;
        console.log(this.user.id);
        this.salt = bcryptjs__WEBPACK_IMPORTED_MODULE_2__["genSaltSync"](16);
    }
    editProfileSubmit(form) {
        console.log(form.value);
        console.log(this.userId);
        for (var prop in form.value) {
            if (form.value[prop] === '') {
                alert("Please fill in all fields");
                return;
            }
        }
        let memSince = this.user.memSince;
        form.value.passHash = bcryptjs__WEBPACK_IMPORTED_MODULE_2__["hashSync"](form.value.passHash, this.salt);
        this.apiService.editProfile(form.value, this.user.id).subscribe(res => {
            console.log(res);
        }, error => console.log(error));
        this.user = new src_models_user__WEBPACK_IMPORTED_MODULE_1__["User"](this.userId, form.value.firstName, form.value.lastName, form.value.username, form.value.email, form.value.phone, memSince);
        localStorage.setItem("user", JSON.stringify(this.user));
        this.router.navigate(['profile']);
    }
}
EditprofileComponent.ɵfac = function EditprofileComponent_Factory(t) { return new (t || EditprofileComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"])); };
EditprofileComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: EditprofileComponent, selectors: [["app-editprofile"]], decls: 24, vars: 1, consts: [["id", "edit", 1, "container", "center"], [1, "title"], [3, "formGroup", "ngSubmit"], [1, "form-group"], ["for", "fname"], ["type", "text", "formControlName", "firstName", "placeholder", "First Name"], ["for", "lname"], ["type", "text", "formControlName", "lastName", "placeholder", "Last Name"], ["for", "username"], ["type", "text", "formControlName", "username", "placeholder", "Username"], ["for", "password"], ["type", "password", "formControlName", "passHash", "placeholder", "Password"], ["for", "email"], ["type", "email", "formControlName", "email", "placeholder", "Email"], ["for", "phone"], ["type", "tel", "formControlName", "phone", "placeholder", "Phone", "required", "", "pattern", "[(][0-9]{3}[)][0-9]{3}-[0-9]{4}", "title", "(123)456-7890"], ["type", "submit", "id", "submit", 1, "mt-4", "btn", "btn-success"]], template: function EditprofileComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Edit Profile");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function EditprofileComponent_Template_form_ngSubmit_3_listener() { return ctx.editProfileSubmit(ctx.editProfile); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "label", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "label", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "label", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "label", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "input", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "label", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "label", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.editProfile);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["PatternValidator"]], styles: ["#edit[_ngcontent-%COMP%] {\r\n  border: 1px solid black;\r\n  padding: 10px -30px;\r\n  width: 300px;\r\n  border-radius: 20px;\r\n}\r\n\r\nform[_ngcontent-%COMP%] {\r\n  padding: 0;\r\n  width: 200px;\r\n  margin: 30px auto;\r\n}\r\n\r\n.title[_ngcontent-%COMP%] {\r\n  color: #04a304;\r\n  margin-top: 20px;\r\n  font-size: 20px;\r\n  margin-bottom: -40px;\r\n}\r\n\r\n#submit[_ngcontent-%COMP%] {\r\n  margin-top: 20px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVkaXRwcm9maWxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLG9CQUFvQjtBQUN0Qjs7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQiIsImZpbGUiOiJlZGl0cHJvZmlsZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2VkaXQge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xyXG4gIHBhZGRpbmc6IDEwcHggLTMwcHg7XHJcbiAgd2lkdGg6IDMwMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbn1cclxuXHJcbmZvcm0ge1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG4gIG1hcmdpbjogMzBweCBhdXRvO1xyXG59XHJcblxyXG4udGl0bGUge1xyXG4gIGNvbG9yOiAjMDRhMzA0O1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IC00MHB4O1xyXG59XHJcbiNzdWJtaXQge1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EditprofileComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-editprofile',
                templateUrl: './editprofile.component.html',
                styleUrls: ['./editprofile.component.css']
            }]
    }], function () { return [{ type: src_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"] }, { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }]; }, null); })();


/***/ }),

/***/ "bzTf":
/*!**********************************************!*\
  !*** ./src/app/contact/contact.component.ts ***!
  \**********************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");


class ContactComponent {
    constructor() { }
    ngOnInit() {
    }
}
ContactComponent.ɵfac = function ContactComponent_Factory(t) { return new (t || ContactComponent)(); };
ContactComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ContactComponent, selectors: [["app-contact"]], decls: 20, vars: 0, consts: [[1, "contact-ctn"], [1, "row"], [1, "col-10"], [1, "about"], [1, "marb"], [1, "mt-2", "marb"], [1, "col-2"], ["src", "../../assets/public-library-mandalore.png", "alt", "", 1, "map"]], template: function ContactComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Public Library of Mandalore");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Location");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " 2739 N Main St Mandalore, 60614 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Hours of Operation");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, " Mon-Fri: 8am - 9pm Sat-Sun: 9am - 7pm ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Phone");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " +32 (4853) 4856-39025");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "img", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".contact-ctn[_ngcontent-%COMP%] {\r\n  border: 1px solid #c5c4c4;\r\n  border-radius: 20px;\r\n  width: 700px;\r\n  padding: 10px;\r\n  background-color: white;\r\n}\r\n.map[_ngcontent-%COMP%] {\r\n  border-radius: 20px;\r\n  height: 265px;\r\n  margin-left: -200px;\r\n}\r\n.marb[_ngcontent-%COMP%] {\r\n  margin-bottom: -2px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRhY3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGFBQWE7RUFDYix1QkFBdUI7QUFDekI7QUFDQTtFQUNFLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSxtQkFBbUI7QUFDckIiLCJmaWxlIjoiY29udGFjdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhY3QtY3RuIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjYzVjNGM0O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgd2lkdGg6IDcwMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbn1cclxuLm1hcCB7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBoZWlnaHQ6IDI2NXB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAtMjAwcHg7XHJcbn1cclxuLm1hcmIge1xyXG4gIG1hcmdpbi1ib3R0b206IC0ycHg7XHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-contact',
                templateUrl: './contact.component.html',
                styleUrls: ['./contact.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "c2A7":
/*!****************************************!*\
  !*** ./src/app/cart/cart.component.ts ***!
  \****************************************/
/*! exports provided: CartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartComponent", function() { return CartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/api.service */ "RcPr");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");





function CartComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function CartComponent_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "No books were found, add some books to your cart!");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
} }
function CartComponent_ng_template_5_div_2_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CartComponent_ng_template_5_div_2_div_1_Template_button_click_13_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r7.remove($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Remove");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const book_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", book_r5.book.image, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r5.book.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r5.book.author);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", book_r5.book.description, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r5.book.isbn);
} }
function CartComponent_ng_template_5_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, CartComponent_ng_template_5_div_2_div_1_Template, 15, 5, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const book_r5 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", book_r5.book);
} }
function CartComponent_ng_template_5_Template(rf, ctx) { if (rf & 1) {
    const _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CartComponent_ng_template_5_Template_button_click_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11); const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r10.checkOut($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Check Out");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, CartComponent_ng_template_5_div_2_Template, 2, 1, "div", 8);
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.books);
} }
class CartComponent {
    constructor(apiService, router) {
        this.apiService = apiService;
        this.router = router;
        this.isSearching = false;
        this.books = [];
        this.fname = '';
        this.booksToSend = [];
    }
    ngOnInit() {
        if (localStorage['user']) {
            this.user = JSON.parse(localStorage['user']);
        }
        else {
            this.router.navigate(['login']);
        }
        this.fname = this.user.fname;
        this.isSearching = true;
        this.apiService.getBooksForCart(this.user.id).subscribe(res => {
            console.log(res);
            this.isSearching = false;
            this.books = res;
            for (let book of res) {
                this.booksToSend.push(book.book);
            }
            console.log(this.booksToSend);
        }, err => {
            this.isSearching = false;
            console.log(err);
        });
    }
    checkOut(e) {
        console.log(this.books);
        this.apiService.addBookAllReserved(this.user.id, this.booksToSend).subscribe(res => console.log(res), err => console.log(err));
        this.apiService.deleteAllFromCart(this.user.id).subscribe(res => {
            console.log('Your books have been reserved');
            alert('Your books have been reserved');
            this.books = [];
        }, error => {
            console.log(error.error);
            alert(error.error);
        });
    }
    remove(e) {
        console.log(e);
        let isbn = e.path[1].children[3].innerText;
        this.apiService.deleteFromCart(isbn, this.user.id).subscribe(res => {
            console.log('Book removed from cart');
            alert('Book removed from cart');
            let index;
            for (var i = 0; i < this.books.length; i++) {
                if (this.books[i].book.isbn == isbn) {
                    index = i;
                }
            }
            this.books.splice(index, 1);
        }, error => {
            console.log(error.error);
            alert(error.error);
        });
    }
}
CartComponent.ɵfac = function CartComponent_Factory(t) { return new (t || CartComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"])); };
CartComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CartComponent, selectors: [["app-cart"]], decls: 7, vars: 4, consts: [["class", "row", 4, "ngIf"], [1, "row"], [4, "ngIf", "ngIfElse"], ["elseTemplate", ""], [1, "col-12", "text-center"], ["src", "../../assets/pageTurner.gif", "alt", "Searching...", 1, "mt-4"], ["role", "alert", 1, "alert", "alert-danger"], [1, "btn", "btn-success", "basket", 3, "click"], ["class", "wrapper", 4, "ngFor", "ngForOf"], [1, "wrapper"], [4, "ngIf"], [1, ""], ["contentEditable", "", "role", "textbox", "aria-multiline", "true", 1, "card"], [3, "src"], [1, "descriptions"], [1, "card-text"], [1, "desc"], [1, "hide"], [1, "btn", "cart", 3, "click"]], template: function CartComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, CartComponent_div_2_Template, 3, 0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, CartComponent_ng_container_4_Template, 5, 0, "ng-container", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, CartComponent_ng_template_5_Template, 3, 1, "ng-template", null, 3, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.fname, "'s Cart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isSearching);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.books.length == 0)("ngIfElse", _r2);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"]], styles: ["body[_ngcontent-%COMP%] {\r\n  \r\n  padding: 0px;\r\n  margin: 0px;\r\n  width: 100%;\r\n  height: 100vh;\r\n  font-family: \"Lato\";\r\n}\r\n.row[_ngcontent-%COMP%] {\r\n  justify-content: center;\r\n}\r\n.wrapper[_ngcontent-%COMP%] {\r\n  \r\n  max-width: 300px;\r\n  width: 100%;\r\n  height: auto;\r\n  display: flex;\r\n  flex-wrap: wrap;\r\n  justify-content: center;\r\n  margin: 10px;\r\n}\r\n.card[_ngcontent-%COMP%] {\r\n  flex: 1;\r\n  flex-basis: 300px;\r\n  flex-grow: 0;\r\n  height: 440px;\r\n  width: 300px;\r\n  background: #fff;\r\n  border: 2px solid #fff;\r\n  box-shadow: 0px 4px 7px rgba(0, 0, 0, 0.5);\r\n  cursor: pointer;\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  overflow: hidden;\r\n  position: relative;\r\n}\r\n.card[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 100%;\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n}\r\n.descriptions[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  top: 0px;\r\n  left: 0px;\r\n  width: 100%;\r\n  height: 100%;\r\n  transition: all 0.7s ease-in-out;\r\n  padding: 20px;\r\n  box-sizing: border-box;\r\n  -webkit-clip-path: circle(0% at 100% 100%);\r\n          clip-path: circle(0% at 100% 100%);\r\n}\r\n.card[_ngcontent-%COMP%]:hover   .descriptions[_ngcontent-%COMP%] {\r\n  left: 0px;\r\n  transition: all 0.7s ease-in-out;\r\n  -webkit-clip-path: circle(75%);\r\n          clip-path: circle(75%);\r\n}\r\n.card[_ngcontent-%COMP%]:hover {\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.3);\r\n  transform: scale(0.97);\r\n}\r\n.card[_ngcontent-%COMP%]:hover   img[_ngcontent-%COMP%] {\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  transform: scale(1.6) rotate(20deg);\r\n  filter: blur(3px);\r\n}\r\n.card[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\r\n  color: white;\r\n  text-shadow: 0 0 5px black;\r\n  letter-spacing: 1px;\r\n  margin: 0px;\r\n  height: 68px;\r\n  overflow: hidden;\r\n}\r\n.card[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  color: white;\r\n  text-shadow: 0 0 5px black;\r\n  line-height: 24px;\r\n  height: 55%;\r\n  font-size: 20px;\r\n}\r\n.card[_ngcontent-%COMP%]:hover   .desc[_ngcontent-%COMP%] {\r\n  background: rgba(54, 54, 54, 0.356);\r\n  border-radius: 10px 0 0 10px;\r\n}\r\n.desc[_ngcontent-%COMP%] {\r\n  height: 150px;\r\n  \r\n  width: 100%;\r\n  overflow-y: scroll;\r\n  overflow-x: hidden;\r\n  padding-right: 50px;\r\n  margin-bottom: 40px;\r\n  box-sizing: content-box;\r\n  mix-blend-mode: difference;\r\n}\r\n.card[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  bottom: 15px;\r\n  margin-left: 80px;\r\n  width: 100px;\r\n  height: 40px;\r\n  cursor: pointer;\r\n  border-style: none;\r\n  background-color: #04a304;\r\n  color: #fff;\r\n  font-size: 18px;\r\n  outline: none;\r\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.4);\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.card[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover {\r\n  transform: scale(0.95) translateX(-5px);\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.hide[_ngcontent-%COMP%] {\r\n  opacity: 0;\r\n}\r\n.cart-button[_ngcontent-%COMP%] {\r\n  max-width: 200px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhcnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLCtCQUErQjtFQUMvQixZQUFZO0VBQ1osV0FBVztFQUNYLFdBQVc7RUFDWCxhQUFhO0VBQ2IsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSx1QkFBdUI7QUFDekI7QUFDQTtFQUNFLHNDQUFzQztFQUN0QyxnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsZUFBZTtFQUNmLHVCQUF1QjtFQUN2QixZQUFZO0FBQ2Q7QUFDQTtFQUNFLE9BQU87RUFDUCxpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGFBQWE7RUFDYixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLHNCQUFzQjtFQUN0QiwwQ0FBMEM7RUFDMUMsZUFBZTtFQUNmLHFEQUFxRDtFQUNyRCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLHFEQUFxRDtBQUN2RDtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixTQUFTO0VBQ1QsV0FBVztFQUNYLFlBQVk7RUFDWixnQ0FBZ0M7RUFDaEMsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QiwwQ0FBa0M7VUFBbEMsa0NBQWtDO0FBQ3BDO0FBQ0E7RUFDRSxTQUFTO0VBQ1QsZ0NBQWdDO0VBQ2hDLDhCQUFzQjtVQUF0QixzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLHFEQUFxRDtFQUNyRCwwQ0FBMEM7RUFDMUMsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSxxREFBcUQ7RUFDckQsbUNBQW1DO0VBQ25DLGlCQUFpQjtBQUNuQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7RUFDWixnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxlQUFlO0FBQ2pCO0FBQ0E7RUFDRSxtQ0FBbUM7RUFDbkMsNEJBQTRCO0FBQzlCO0FBQ0E7RUFDRSxhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLDBCQUEwQjtBQUM1QjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLFlBQVk7RUFDWixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixXQUFXO0VBQ1gsZUFBZTtFQUNmLGFBQWE7RUFDYiwwQ0FBMEM7RUFDMUMsZ0NBQWdDO0FBQ2xDO0FBRUE7RUFDRSx1Q0FBdUM7RUFDdkMsZ0NBQWdDO0FBQ2xDO0FBRUE7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQiIsImZpbGUiOiJjYXJ0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJib2R5IHtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiAjM2QzZDNkOyAqL1xyXG4gIHBhZGRpbmc6IDBweDtcclxuICBtYXJnaW46IDBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMHZoO1xyXG4gIGZvbnQtZmFtaWx5OiBcIkxhdG9cIjtcclxufVxyXG4ucm93IHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG4ud3JhcHBlciB7XHJcbiAgLyogdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7ICovXHJcbiAgbWF4LXdpZHRoOiAzMDBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAxMHB4O1xyXG59XHJcbi5jYXJkIHtcclxuICBmbGV4OiAxO1xyXG4gIGZsZXgtYmFzaXM6IDMwMHB4O1xyXG4gIGZsZXgtZ3JvdzogMDtcclxuICBoZWlnaHQ6IDQ0MHB4O1xyXG4gIHdpZHRoOiAzMDBweDtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIGJvcmRlcjogMnB4IHNvbGlkICNmZmY7XHJcbiAgYm94LXNoYWRvdzogMHB4IDRweCA3cHggcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBjdWJpYy1iZXppZXIoMC44LCAwLjUsIDAuMiwgMS40KTtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FyZCBpbWcge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBjdWJpYy1iZXppZXIoMC44LCAwLjUsIDAuMiwgMS40KTtcclxufVxyXG4uZGVzY3JpcHRpb25zIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAwcHg7XHJcbiAgbGVmdDogMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC43cyBlYXNlLWluLW91dDtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgY2xpcC1wYXRoOiBjaXJjbGUoMCUgYXQgMTAwJSAxMDAlKTtcclxufVxyXG4uY2FyZDpob3ZlciAuZGVzY3JpcHRpb25zIHtcclxuICBsZWZ0OiAwcHg7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuN3MgZWFzZS1pbi1vdXQ7XHJcbiAgY2xpcC1wYXRoOiBjaXJjbGUoNzUlKTtcclxufVxyXG4uY2FyZDpob3ZlciB7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgY3ViaWMtYmV6aWVyKDAuOCwgMC41LCAwLjIsIDEuNCk7XHJcbiAgYm94LXNoYWRvdzogMHB4IDJweCAzcHggcmdiYSgwLCAwLCAwLCAwLjMpO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMC45Nyk7XHJcbn1cclxuLmNhcmQ6aG92ZXIgaW1nIHtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBjdWJpYy1iZXppZXIoMC44LCAwLjUsIDAuMiwgMS40KTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDEuNikgcm90YXRlKDIwZGVnKTtcclxuICBmaWx0ZXI6IGJsdXIoM3B4KTtcclxufVxyXG4uY2FyZCBoMyB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHRleHQtc2hhZG93OiAwIDAgNXB4IGJsYWNrO1xyXG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbiAgaGVpZ2h0OiA2OHB4O1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuLmNhcmQgcCB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHRleHQtc2hhZG93OiAwIDAgNXB4IGJsYWNrO1xyXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xyXG4gIGhlaWdodDogNTUlO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxufVxyXG4uY2FyZDpob3ZlciAuZGVzYyB7XHJcbiAgYmFja2dyb3VuZDogcmdiYSg1NCwgNTQsIDU0LCAwLjM1Nik7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweCAwIDAgMTBweDtcclxufVxyXG4uZGVzYyB7XHJcbiAgaGVpZ2h0OiAxNTBweDtcclxuICAvKiBoZWlnaHQ6IDEwMCU7ICovXHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuICBwYWRkaW5nLXJpZ2h0OiA1MHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XHJcbiAgYm94LXNpemluZzogY29udGVudC1ib3g7XHJcbiAgbWl4LWJsZW5kLW1vZGU6IGRpZmZlcmVuY2U7XHJcbn1cclxuXHJcbi5jYXJkIGJ1dHRvbiB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvdHRvbTogMTVweDtcclxuICBtYXJnaW4tbGVmdDogODBweDtcclxuICB3aWR0aDogMTAwcHg7XHJcbiAgaGVpZ2h0OiA0MHB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBib3JkZXItc3R5bGU6IG5vbmU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA0YTMwNDtcclxuICBjb2xvcjogI2ZmZjtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICBib3gtc2hhZG93OiAwcHggMnB4IDNweCByZ2JhKDAsIDAsIDAsIDAuNCk7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZS1pbi1vdXQ7XHJcbn1cclxuXHJcbi5jYXJkIGJ1dHRvbjpob3ZlciB7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgwLjk1KSB0cmFuc2xhdGVYKC01cHgpO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG4uaGlkZSB7XHJcbiAgb3BhY2l0eTogMDtcclxufVxyXG4uY2FydC1idXR0b24ge1xyXG4gIG1heC13aWR0aDogMjAwcHg7XHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CartComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-cart',
                templateUrl: './cart.component.html',
                styleUrls: ['./cart.component.css']
            }]
    }], function () { return [{ type: src_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }]; }, null); })();


/***/ }),

/***/ "iBUs":
/*!******************************************************!*\
  !*** ./src/app/staff-faves/staff-faves.component.ts ***!
  \******************************************************/
/*! exports provided: StaffFavesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffFavesComponent", function() { return StaffFavesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var src_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/api.service */ "RcPr");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");





class StaffFavesComponent {
    constructor(http, apiService, router) {
        this.http = http;
        this.apiService = apiService;
        this.router = router;
        this.isSearching = false;
        this.addedToCart = false;
        this.addedToWishlist = false;
        this.apiResponse = [];
        if (localStorage['user']) {
            this.user = JSON.parse(localStorage['user']);
        }
    }
    ngOnInit() {
    }
    addToCart(e) {
        console.log(e);
        if (!localStorage.getItem('user')) {
            this.router.navigate(['login']);
        }
        let image = e.path[2].children[0].currentSrc;
        let isbn = e.path[1].children[5].innerText;
        let author = e.path[2].childNodes[1].childNodes[1].innerText;
        let title = e.path[2].childNodes[1].childNodes[0].innerText;
        let desc = e.path[1].childNodes[2].innerText;
        let book = { "title": title, "author": author, "image": image, "isbn": isbn, "description": desc };
        console.log(book);
        this.apiService.addBookToCart(this.user.id, book).subscribe(res => {
            console.log(res);
            setTimeout(() => {
                this.addedToCart = true;
            }, 2000);
        }, error => console.log(error));
    }
    ;
    addToWishlist(e) {
        this.addedToWishlist = true;
        console.log(e);
        if (!localStorage.getItem('user')) {
            this.router.navigate(['login']);
        }
        let image = e.path[2].children[0].currentSrc;
        let isbn = e.path[1].children[3].innerText;
        let author = e.path[2].childNodes[1].childNodes[1].innerText;
        let title = e.path[2].childNodes[1].childNodes[0].innerText;
        let desc = e.path[1].childNodes[2].innerText;
        let book = { "title": title, "author": author, "image": image, "isbn": isbn, "description": desc };
        console.log(book);
        this.apiService.addBookToWishlist(this.user.id, book).subscribe(res => {
            console.log(res);
            setTimeout(() => {
                this.addedToWishlist = true;
            }, 2000);
        }, error => console.log(error));
    }
}
StaffFavesComponent.ɵfac = function StaffFavesComponent_Factory(t) { return new (t || StaffFavesComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"])); };
StaffFavesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: StaffFavesComponent, selectors: [["app-staff-faves"]], decls: 106, vars: 0, consts: [[1, "staff-ctn"], [1, "staff-title", "center"], [1, "content-slider"], [1, "slider"], [1, "mask"], [1, "anim1"], [1, "card"], ["src", "http://books.google.com/books/content?id=1Kw1elu9QjAC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"], [1, "descriptions"], [1, "title"], [1, "card-text"], [1, "desc"], [1, "hide"], [1, "btn", "cart", 3, "click"], [1, "btn", "wish", 3, "click"], [1, "anim2"], ["src", "http://books.google.com/books/content?id=g0OZtYOW1v8C&printsec=frontcover&img=1&zoom=1&source=gbs_api "], [1, "anim3"], ["src", "http://books.google.com/books/content?id=FKzLugEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api"], [1, "anim4"], ["src", "http://books.google.com/books/content?id=zV4PuFn5gvoC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"], [1, "anim5"], ["src", "http://books.google.com/books/content?id=FOHtDwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"], [1, "anim6"], ["src", "http://books.google.com/books/content?id=XfFvDwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"], [1, "staff-text"]], template: function StaffFavesComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Staff Picks");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "ul");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "li", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "img", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "h5", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Paint It Black");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Janet Fitch");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " \"A dark, crooked beauty that fulfills all the promise of White Oleander and confirms that Janet Fitch is an artist of the very highest order.\" --Los Angeles Times Book Review Josie Tyrell, art model, runaway, and denizen of LA's rock scene finds a chance at real love with Michael Faraday, a Harvard dropout and son of a renowned pianist. But when she receives a call from the coroner, asking her to identify her lover's body, her bright dreams all turn to black. As Josie struggles to understand Michael's death and to hold onto the world they shared, she is both attracted to and repelled by his pianist mother, Meredith, who blames Josie for her son's torment. Soon the two women are drawn into a twisted relationship that reflects equal parts distrust and blind need. With the luxurious prose and fever pitch intensity that are her hallmarks, Janet Fitch weaves a spellbinding tale of love, betrayal, and the possibility of transcendence. \"Lushly written, dramatically plotted. . . Fitch's Los Angeles is so real it breathes.\" --Atlantic Monthly \"There is nothing less than a stellar sentence in this novel. Fitch's emotional honesty recalls the work of Joyce Carol Oates, her strychnine sentences the prose of Paula Fox.\" --Cleveland Plain Dealer \"A page-turning psychodrama. . . . Fitch's prose penetrates the inner lives of [her characters] with immediacy and bite.\" --Publishers Weekly (starred review) \"Fitch wonderfully captures the abrasive appeal of punk music, the bohemian, sometimes squalid lifestyle, the performers, the drugs, the alienation. This is crackling fresh stuff you don't read every day.\" --USA Today \"In dysfunctional family narratives, Fitch is to fiction what Eugene O'Neill is to drama.\" --Chicago Sun-Times \"Riveting. . . . An uncommonly accomplished page-turner.\" --Elle ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "075956812X");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_20_listener($event) { return ctx.addToCart($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Cart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_22_listener($event) { return ctx.addToWishlist($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Wishlist");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "li", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "h5", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, " A Storm of Swords Complete Edition (Two in One) (A Song of Ice and Fire, Book 3)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "George R. R. Martin");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, " HBO's hit series A GAME OF THRONES is based on George R. R. Martin's internationally bestselling series A SONG OF ICE AND FIRE, the greatest fantasy epic of the modern age. A STORM OF SWORDS is the third volume in the series. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "9780007426232");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_36_listener($event) { return ctx.addToCart($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Cart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_38_listener($event) { return ctx.addToWishlist($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Wishlist");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "li", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "img", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "h5", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " 1066");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "David Armine Howarth");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, " An account of the year's events in England, from the death of Edward the Confessor, through the struggle for succession, to the Christmas coronation of William of Normandy, focuses on the Norman invasion and the Battle of Hastings ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "0140058508");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_52_listener($event) { return ctx.addToCart($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Cart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_54_listener($event) { return ctx.addToWishlist($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Wishlist");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "li", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "img", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "h5", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Sex at Dawn");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Christopher Ryan,Cacilda Jetha");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, " \"Since Darwin's day, we've been told that sexual monogamy comes naturally to our species. Mainstream science\u2014as well as religious and cultural institutions\u2014has maintained that men and women evolved in families in which a man's possessions and protection were exchanged for a woman's fertility and fidelity. But this narrative is collapsing. Fewer and fewer couples are getting married, and divorce rates keep climbing as adultery and flagging libido drag down even seemingly solid marriages. How can reality be reconciled with the accepted narrative? It can't be, according to renegade thinkers Christopher Ryan and Cacilda Jeth\u00E5. While debunking almost everything we \"know\" about sex, they offer a bold alternative explanation in this provocative and brilliant book. Ryan and Jeth\u00E5's central contention is that human beings evolved in egalitarian groups that shared food, child care, and, often, sexual partners. Weaving together convergent, frequently overlooked evidence from anthropology, archaeology, primatology, anatomy, and psychosexuality, the authors show how far from human nature monogamy really is. Human beings everywhere and in every era have confronted the same familiar, intimate situations in surprisingly different ways. The authors expose the ancient roots of human sexuality while pointing toward a more optimistic future illuminated by our innate capacities for love, cooperation, and generosity. With intelligence, humor, and wonder, Ryan and Jeth\u00E5 show how our promiscuous past haunts our struggles over monogamy, sexual orientation, and family dynamics. They explore why long-term fidelity can be so difficult for so many; why sexual passion tends to fade even as love deepens; why many middle-aged men risk everything for transient affairs with younger women; why homosexuality persists in the face of standard evolutionary logic; and what the human body reveals about the prehistoric origins of modern sexuality. In the tradition of the best historical and scientific writing, Sex at Dawn unapologetically upends unwarranted assumptions and unfounded conclusions while offering a revolutionary understanding of why we live and love as we do. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "075956812X");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_68_listener($event) { return ctx.addToCart($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Cart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_70_listener($event) { return ctx.addToWishlist($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Wishlist");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "li", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "img", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "h5", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Ready Player Two");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "Ernest Cline");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, " #1 NEW YORK TIMES BESTSELLER \u00E2\u0080\u00A2 The highly anticipated sequel to the beloved worldwide bestseller Ready Player One, the near-future adventure that inspired the blockbuster Steven Spielberg film. NAMED ONE OF THE BEST BOOKS OF THE YEAR BY THE WASHINGTON POST AN UNEXPECTED QUEST. TWO WORLDS AT STAKE. ARE YOU READY? Days after winning OASIS founder James Halliday\u00E2\u0080\u0099s contest, Wade Watts makes a discovery that changes everything. Hidden within Halliday\u00E2\u0080\u0099s vaults, waiting for his heir to find, lies a technological advancement that will once again change the world and make the OASIS a thousand times more wondrous\u00E2\u0080\u0094and addictive\u00E2\u0080\u0094than even Wade dreamed possible. With it comes a new riddle, and a new quest\u00E2\u0080\u0094a last Easter egg from Halliday, hinting at a mysterious prize. And an unexpected, impossibly powerful, and dangerous new rival awaits, one who'll kill millions to get what he wants. Wade\u00E2\u0080\u0099s life and the future of the OASIS are again at stake, but this time the fate of humanity also hangs in the balance. Lovingly nostalgic and wildly original as only Ernest Cline could conceive it, Ready Player Two takes us on another imaginative, fun, action-packed adventure through his beloved virtual universe, and jolts us thrillingly into the future once again. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "9781524761356");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_84_listener($event) { return ctx.addToCart($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "Cart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_86_listener($event) { return ctx.addToWishlist($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Wishlist");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "li", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](90, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "h5", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "Atomic Habits");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](95, "James Clear");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, " James Clear presents strategies to form good habits, break bad ones, and master the tiny behaviors that help lead to an improved life. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, "9780735211292");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_100_listener($event) { return ctx.addToCart($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "Cart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StaffFavesComponent_Template_button_click_102_listener($event) { return ctx.addToWishlist($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Wishlist");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, " Here is a collection of our staff's favorite reads. All amazing titles that will be difficult to put down. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".staff-title[_ngcontent-%COMP%] {\r\n  margin-top: 10px;\r\n}\r\n.staff-ctn[_ngcontent-%COMP%] {\r\n  border: 1px solid #c5c4c4;\r\n  border-radius: 20px;\r\n  margin: 0 50px 60px 0;\r\n  background: white;\r\n}\r\n.staff-text[_ngcontent-%COMP%] {\r\n  padding: 10px 50px;\r\n}\r\n.card-ctn[_ngcontent-%COMP%] {\r\n  width: 100vw;\r\n  height: 300px;\r\n}\r\n.carousel-wrapper[_ngcontent-%COMP%] {\r\n  height: 400px;\r\n  position: relative;\r\n  width: 800px;\r\n  margin: 0 auto;\r\n}\r\n.carousel-item[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n  padding: 25px 50px;\r\n  opacity: 0;\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.arrow[_ngcontent-%COMP%] {\r\n  border: solid black;\r\n  border-width: 0 3px 3px 0;\r\n  display: inline-block;\r\n  padding: 12px;\r\n}\r\n.arrow-prev[_ngcontent-%COMP%] {\r\n  left: -30px;\r\n  position: absolute;\r\n  top: 50%;\r\n  transform: translateY(-50%) rotate(135deg);\r\n}\r\n.arrow-next[_ngcontent-%COMP%] {\r\n  right: -30px;\r\n  position: absolute;\r\n  top: 50%;\r\n  transform: translateY(-50%) rotate(-45deg);\r\n}\r\n.light[_ngcontent-%COMP%] {\r\n  color: white;\r\n}\r\n\r\n.content-slider[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 315px;\r\n}\r\n.slider[_ngcontent-%COMP%] {\r\n  height: 320px;\r\n  width: 320px;\r\n  margin: 0px auto 0;\r\n  overflow: visible;\r\n  position: relative;\r\n}\r\n.mask[_ngcontent-%COMP%] {\r\n  overflow: hidden;\r\n  height: 320px;\r\n  margin-left: 8%;\r\n}\r\n.slider[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  padding: 0;\r\n  position: relative;\r\n}\r\n.slider[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n  width: 320px;\r\n  height: 320px;\r\n  position: absolute;\r\n  left: -325px;\r\n  list-style: none;\r\n}\r\n.slider[_ngcontent-%COMP%]   .quote[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-style: italic;\r\n}\r\n.slider[_ngcontent-%COMP%]   .source[_ngcontent-%COMP%] {\r\n  font-size: 20px;\r\n  text-align: right;\r\n}\r\n.slider[_ngcontent-%COMP%]   li.anim1[_ngcontent-%COMP%] {\r\n  animation: cycle 15s linear infinite;\r\n}\r\n.slider[_ngcontent-%COMP%]   li.anim2[_ngcontent-%COMP%] {\r\n  animation: cycle2 15s linear infinite;\r\n}\r\n.slider[_ngcontent-%COMP%]   li.anim3[_ngcontent-%COMP%] {\r\n  animation: cycle3 15s linear infinite;\r\n}\r\n.slider[_ngcontent-%COMP%]   li.anim4[_ngcontent-%COMP%] {\r\n  animation: cycle4 15s linear infinite;\r\n}\r\n.slider[_ngcontent-%COMP%]   li.anim5[_ngcontent-%COMP%] {\r\n  animation: cycle5 15s linear infinite;\r\n}\r\n.slider[_ngcontent-%COMP%]   li.anim6[_ngcontent-%COMP%] {\r\n  animation: cycle6 15s linear infinite;\r\n}\r\n.slider[_ngcontent-%COMP%]   li.anim7[_ngcontent-%COMP%] {\r\n  animation: cycle7 15s linear infinite;\r\n}\r\n.slider[_ngcontent-%COMP%]:hover   li[_ngcontent-%COMP%] {\r\n  animation-play-state: paused;\r\n}\r\n@keyframes cycle {\r\n  0% {\r\n    left: 0px;\r\n  }\r\n  4% {\r\n    left: 0px;\r\n  }\r\n  16% {\r\n    left: 0px;\r\n    opacity: 1;\r\n    z-index: 0;\r\n  }\r\n  20% {\r\n    left: 325px;\r\n    opacity: 0;\r\n    z-index: 0;\r\n  }\r\n  21% {\r\n    left: -325px;\r\n    opacity: 0;\r\n    z-index: -1;\r\n  }\r\n  50% {\r\n    left: -325px;\r\n    opacity: 0;\r\n    z-index: -1;\r\n  }\r\n  92% {\r\n    left: -325px;\r\n    opacity: 0;\r\n    z-index: 0;\r\n  }\r\n  96% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  100% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n}\r\n@keyframes cycle2 {\r\n  0% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  16% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  20% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  24% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  36% {\r\n    left: 0px;\r\n    opacity: 1;\r\n    z-index: 0;\r\n  }\r\n  40% {\r\n    left: 325px;\r\n    opacity: 0;\r\n    z-index: 0;\r\n  }\r\n  41% {\r\n    left: -325px;\r\n    opacity: 0;\r\n    z-index: -1;\r\n  }\r\n  100% {\r\n    left: -325px;\r\n    opacity: 0;\r\n    z-index: -1;\r\n  }\r\n}\r\n@keyframes cycle3 {\r\n  0% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  36% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  40% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  44% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  56% {\r\n    left: 0px;\r\n    opacity: 1;\r\n    z-index: 0;\r\n  }\r\n  60% {\r\n    left: 325px;\r\n    opacity: 0;\r\n    z-index: 0;\r\n  }\r\n  61% {\r\n    left: -325px;\r\n    opacity: 0;\r\n    z-index: -1;\r\n  }\r\n  100% {\r\n    left: -325px;\r\n    opacity: 0;\r\n    z-index: -1;\r\n  }\r\n}\r\n@keyframes cycle4 {\r\n  0% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  56% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  60% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  64% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  76% {\r\n    left: 0px;\r\n    opacity: 1;\r\n    z-index: 0;\r\n  }\r\n  80% {\r\n    left: 325px;\r\n    opacity: 0;\r\n    z-index: 0;\r\n  }\r\n  81% {\r\n    left: -325px;\r\n    opacity: 0;\r\n    z-index: -1;\r\n  }\r\n  100% {\r\n    left: -325px;\r\n    opacity: 0;\r\n    z-index: -1;\r\n  }\r\n}\r\n@keyframes cycle5 {\r\n  0% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  76% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  80% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  84% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  96% {\r\n    left: 0px;\r\n    opacity: 1;\r\n    z-index: 0;\r\n  }\r\n  100% {\r\n    left: 325px;\r\n    opacity: 0;\r\n    z-index: 0;\r\n  }\r\n}\r\n@keyframes cycle6 {\r\n  0% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  76% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  80% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  84% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  96% {\r\n    left: 0px;\r\n    opacity: 1;\r\n    z-index: 0;\r\n  }\r\n  100% {\r\n    left: 325px;\r\n    opacity: 0;\r\n    z-index: 0;\r\n  }\r\n}\r\n@keyframes cycle7 {\r\n  0% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  76% {\r\n    left: -325px;\r\n    opacity: 0;\r\n  }\r\n  80% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  84% {\r\n    left: 0px;\r\n    opacity: 1;\r\n  }\r\n  96% {\r\n    left: 0px;\r\n    opacity: 1;\r\n    z-index: 0;\r\n  }\r\n  100% {\r\n    left: 325px;\r\n    opacity: 0;\r\n    z-index: 0;\r\n  }\r\n}\r\n\r\n\r\n.card[_ngcontent-%COMP%] {\r\n  flex: 1;\r\n  flex-basis: 300px;\r\n  flex-grow: 0;\r\n  height: 300px;\r\n  width: 200px;\r\n  background: #fff;\r\n  border: 2px solid #fff;\r\n  box-shadow: 0px 4px 7px rgba(0, 0, 0, 0.5);\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  overflow: hidden;\r\n  position: relative;\r\n  margin-left: 20px;\r\n}\r\n.card[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 100%;\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n}\r\n.descriptions[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  top: 0px;\r\n  left: 0px;\r\n  width: 100%;\r\n  height: 100%;\r\n  transition: all 0.7s ease-in-out;\r\n  padding: 20px;\r\n  box-sizing: border-box;\r\n  -webkit-clip-path: circle(0% at 100% 100%);\r\n          clip-path: circle(0% at 100% 100%);\r\n}\r\n.card[_ngcontent-%COMP%]:hover   .descriptions[_ngcontent-%COMP%] {\r\n  left: 0px;\r\n  transition: all 0.7s ease-in-out;\r\n  -webkit-clip-path: circle(75%);\r\n          clip-path: circle(75%);\r\n}\r\n.card[_ngcontent-%COMP%]:hover {\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.3);\r\n  transform: scale(0.97);\r\n}\r\n.card[_ngcontent-%COMP%]:hover   img[_ngcontent-%COMP%] {\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  transform: scale(1.6) rotate(20deg);\r\n  filter: blur(3px);\r\n}\r\n.card[_ngcontent-%COMP%]   h5[_ngcontent-%COMP%] {\r\n  color: white;\r\n  text-shadow: 0 0 5px black;\r\n  letter-spacing: 1px;\r\n  margin: 0px;\r\n  margin-top: -20px;\r\n  height: 45px;\r\n  overflow: hidden;\r\n}\r\n.card[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  color: white;\r\n  text-shadow: 0 0 5px black;\r\n  line-height: 24px;\r\n  font-size: 15px;\r\n}\r\n.card-text[_ngcontent-%COMP%] {\r\n  height: 47px;\r\n}\r\n.card[_ngcontent-%COMP%]:hover   .desc[_ngcontent-%COMP%] {\r\n  background: rgba(54, 54, 54, 0.356);\r\n  border-radius: 10px 0 0 10px;\r\n}\r\n.desc[_ngcontent-%COMP%] {\r\n  height: 100px;\r\n  height: 147px;\r\n  width: 100%;\r\n  overflow-y: scroll;\r\n  padding-right: 40px;\r\n  margin-bottom: 20px;\r\n  margin-top: -12px;\r\n  box-sizing: content-box;\r\n  mix-blend-mode: difference;\r\n}\r\n.card[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  bottom: 15px;\r\n  width: 70px;\r\n  height: 30px;\r\n  cursor: pointer;\r\n  border-style: none;\r\n  background-color: #04a304;\r\n  color: #fff;\r\n  font-size: 13px;\r\n  outline: none;\r\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.4);\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.wish[_ngcontent-%COMP%] {\r\n  right: 10px;\r\n}\r\n.cart[_ngcontent-%COMP%] {\r\n  left: 10px;\r\n}\r\n.card[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover {\r\n  transform: scale(0.95) translateX(-5px);\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.hide[_ngcontent-%COMP%] {\r\n  opacity: 0;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0YWZmLWZhdmVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLGlCQUFpQjtBQUNuQjtBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCO0FBRUE7RUFDRSxZQUFZO0VBQ1osYUFBYTtBQUNmO0FBRUE7RUFDRSxhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLFNBQVM7RUFDVCxPQUFPO0VBQ1AsUUFBUTtFQUNSLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsZ0NBQWdDO0FBQ2xDO0FBQ0E7RUFDRSxtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLHFCQUFxQjtFQUNyQixhQUFhO0FBQ2Y7QUFFQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLDBDQUEwQztBQUM1QztBQUVBO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsMENBQTBDO0FBQzVDO0FBRUE7RUFDRSxZQUFZO0FBQ2Q7QUFFQTs7OEJBRThCO0FBQzlCO0VBQ0UsV0FBVztFQUNYLGFBQWE7QUFDZjtBQUVBO0VBQ0UsYUFBYTtFQUNiLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixlQUFlO0FBQ2pCO0FBRUE7RUFDRSxTQUFTO0VBQ1QsVUFBVTtFQUNWLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGdCQUFnQjtBQUNsQjtBQUVBO0VBQ0UsZUFBZTtFQUNmLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQjtBQUNuQjtBQUVBO0VBQ0Usb0NBQW9DO0FBQ3RDO0FBRUE7RUFDRSxxQ0FBcUM7QUFDdkM7QUFFQTtFQUNFLHFDQUFxQztBQUN2QztBQUVBO0VBQ0UscUNBQXFDO0FBQ3ZDO0FBRUE7RUFDRSxxQ0FBcUM7QUFDdkM7QUFFQTtFQUNFLHFDQUFxQztBQUN2QztBQUVBO0VBQ0UscUNBQXFDO0FBQ3ZDO0FBRUE7RUFDRSw0QkFBNEI7QUFDOUI7QUFFQTtFQUNFO0lBQ0UsU0FBUztFQUNYO0VBQ0E7SUFDRSxTQUFTO0VBQ1g7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0lBQ1YsVUFBVTtFQUNaO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLFVBQVU7RUFDWjtFQUNBO0lBQ0UsWUFBWTtJQUNaLFVBQVU7SUFDVixXQUFXO0VBQ2I7RUFDQTtJQUNFLFlBQVk7SUFDWixVQUFVO0lBQ1YsV0FBVztFQUNiO0VBQ0E7SUFDRSxZQUFZO0lBQ1osVUFBVTtJQUNWLFVBQVU7RUFDWjtFQUNBO0lBQ0UsWUFBWTtJQUNaLFVBQVU7RUFDWjtFQUNBO0lBQ0UsU0FBUztJQUNULFVBQVU7RUFDWjtBQUNGO0FBRUE7RUFDRTtJQUNFLFlBQVk7SUFDWixVQUFVO0VBQ1o7RUFDQTtJQUNFLFlBQVk7SUFDWixVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0lBQ1YsVUFBVTtFQUNaO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLFVBQVU7RUFDWjtFQUNBO0lBQ0UsWUFBWTtJQUNaLFVBQVU7SUFDVixXQUFXO0VBQ2I7RUFDQTtJQUNFLFlBQVk7SUFDWixVQUFVO0lBQ1YsV0FBVztFQUNiO0FBQ0Y7QUFFQTtFQUNFO0lBQ0UsWUFBWTtJQUNaLFVBQVU7RUFDWjtFQUNBO0lBQ0UsWUFBWTtJQUNaLFVBQVU7RUFDWjtFQUNBO0lBQ0UsU0FBUztJQUNULFVBQVU7RUFDWjtFQUNBO0lBQ0UsU0FBUztJQUNULFVBQVU7RUFDWjtFQUNBO0lBQ0UsU0FBUztJQUNULFVBQVU7SUFDVixVQUFVO0VBQ1o7RUFDQTtJQUNFLFdBQVc7SUFDWCxVQUFVO0lBQ1YsVUFBVTtFQUNaO0VBQ0E7SUFDRSxZQUFZO0lBQ1osVUFBVTtJQUNWLFdBQVc7RUFDYjtFQUNBO0lBQ0UsWUFBWTtJQUNaLFVBQVU7SUFDVixXQUFXO0VBQ2I7QUFDRjtBQUVBO0VBQ0U7SUFDRSxZQUFZO0lBQ1osVUFBVTtFQUNaO0VBQ0E7SUFDRSxZQUFZO0lBQ1osVUFBVTtFQUNaO0VBQ0E7SUFDRSxTQUFTO0lBQ1QsVUFBVTtFQUNaO0VBQ0E7SUFDRSxTQUFTO0lBQ1QsVUFBVTtFQUNaO0VBQ0E7SUFDRSxTQUFTO0lBQ1QsVUFBVTtJQUNWLFVBQVU7RUFDWjtFQUNBO0lBQ0UsV0FBVztJQUNYLFVBQVU7SUFDVixVQUFVO0VBQ1o7RUFDQTtJQUNFLFlBQVk7SUFDWixVQUFVO0lBQ1YsV0FBVztFQUNiO0VBQ0E7SUFDRSxZQUFZO0lBQ1osVUFBVTtJQUNWLFdBQVc7RUFDYjtBQUNGO0FBRUE7RUFDRTtJQUNFLFlBQVk7SUFDWixVQUFVO0VBQ1o7RUFDQTtJQUNFLFlBQVk7SUFDWixVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0lBQ1YsVUFBVTtFQUNaO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLFVBQVU7RUFDWjtBQUNGO0FBRUE7RUFDRTtJQUNFLFlBQVk7SUFDWixVQUFVO0VBQ1o7RUFDQTtJQUNFLFlBQVk7SUFDWixVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0lBQ1YsVUFBVTtFQUNaO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLFVBQVU7RUFDWjtBQUNGO0FBQ0E7RUFDRTtJQUNFLFlBQVk7SUFDWixVQUFVO0VBQ1o7RUFDQTtJQUNFLFlBQVk7SUFDWixVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0VBQ1o7RUFDQTtJQUNFLFNBQVM7SUFDVCxVQUFVO0lBQ1YsVUFBVTtFQUNaO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLFVBQVU7RUFDWjtBQUNGO0FBRUE7OzhCQUU4QjtBQUU5Qjs7OEJBRThCO0FBQzlCO0VBQ0UsT0FBTztFQUNQLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osYUFBYTtFQUNiLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsc0JBQXNCO0VBQ3RCLDBDQUEwQztFQUMxQyxxREFBcUQ7RUFDckQsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1oscURBQXFEO0FBQ3ZEO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFNBQVM7RUFDVCxXQUFXO0VBQ1gsWUFBWTtFQUNaLGdDQUFnQztFQUNoQyxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLDBDQUFrQztVQUFsQyxrQ0FBa0M7QUFDcEM7QUFDQTtFQUNFLFNBQVM7RUFDVCxnQ0FBZ0M7RUFDaEMsOEJBQXNCO1VBQXRCLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UscURBQXFEO0VBQ3JELDBDQUEwQztFQUMxQyxzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLHFEQUFxRDtFQUNyRCxtQ0FBbUM7RUFDbkMsaUJBQWlCO0FBQ25CO0FBQ0E7RUFDRSxZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixnQkFBZ0I7QUFDbEI7QUFFQTtFQUNFLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsaUJBQWlCO0VBQ2pCLGVBQWU7QUFDakI7QUFDQTtFQUNFLFlBQVk7QUFDZDtBQUNBO0VBQ0UsbUNBQW1DO0VBQ25DLDRCQUE0QjtBQUM5QjtBQUNBO0VBQ0UsYUFBYTtFQUNiLGFBQWE7RUFDYixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLHVCQUF1QjtFQUN2QiwwQkFBMEI7QUFDNUI7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixXQUFXO0VBQ1gsZUFBZTtFQUNmLGFBQWE7RUFDYiwwQ0FBMEM7RUFDMUMsZ0NBQWdDO0FBQ2xDO0FBQ0E7RUFDRSxXQUFXO0FBQ2I7QUFDQTtFQUNFLFVBQVU7QUFDWjtBQUVBO0VBQ0UsdUNBQXVDO0VBQ3ZDLGdDQUFnQztBQUNsQztBQUVBO0VBQ0UsVUFBVTtBQUNaIiwiZmlsZSI6InN0YWZmLWZhdmVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3RhZmYtdGl0bGUge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuLnN0YWZmLWN0biB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2M1YzRjNDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIG1hcmdpbjogMCA1MHB4IDYwcHggMDtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxufVxyXG5cclxuLnN0YWZmLXRleHQge1xyXG4gIHBhZGRpbmc6IDEwcHggNTBweDtcclxufVxyXG5cclxuLmNhcmQtY3RuIHtcclxuICB3aWR0aDogMTAwdnc7XHJcbiAgaGVpZ2h0OiAzMDBweDtcclxufVxyXG5cclxuLmNhcm91c2VsLXdyYXBwZXIge1xyXG4gIGhlaWdodDogNDAwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHdpZHRoOiA4MDBweDtcclxuICBtYXJnaW46IDAgYXV0bztcclxufVxyXG4uY2Fyb3VzZWwtaXRlbSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICBib3R0b206IDA7XHJcbiAgbGVmdDogMDtcclxuICByaWdodDogMDtcclxuICBwYWRkaW5nOiAyNXB4IDUwcHg7XHJcbiAgb3BhY2l0eTogMDtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlLWluLW91dDtcclxufVxyXG4uYXJyb3cge1xyXG4gIGJvcmRlcjogc29saWQgYmxhY2s7XHJcbiAgYm9yZGVyLXdpZHRoOiAwIDNweCAzcHggMDtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgcGFkZGluZzogMTJweDtcclxufVxyXG5cclxuLmFycm93LXByZXYge1xyXG4gIGxlZnQ6IC0zMHB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDUwJTtcclxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSkgcm90YXRlKDEzNWRlZyk7XHJcbn1cclxuXHJcbi5hcnJvdy1uZXh0IHtcclxuICByaWdodDogLTMwcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogNTAlO1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKSByb3RhdGUoLTQ1ZGVnKTtcclxufVxyXG5cclxuLmxpZ2h0IHtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi8qICoqKioqKioqKioqKioqKioqKioqKioqKipcclxuICAgICAgIENhcmQgQ2Fyb3VzZWxcclxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKiAqL1xyXG4uY29udGVudC1zbGlkZXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMzE1cHg7XHJcbn1cclxuXHJcbi5zbGlkZXIge1xyXG4gIGhlaWdodDogMzIwcHg7XHJcbiAgd2lkdGg6IDMyMHB4O1xyXG4gIG1hcmdpbjogMHB4IGF1dG8gMDtcclxuICBvdmVyZmxvdzogdmlzaWJsZTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5tYXNrIHtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIGhlaWdodDogMzIwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDglO1xyXG59XHJcblxyXG4uc2xpZGVyIHVsIHtcclxuICBtYXJnaW46IDA7XHJcbiAgcGFkZGluZzogMDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5zbGlkZXIgbGkge1xyXG4gIHdpZHRoOiAzMjBweDtcclxuICBoZWlnaHQ6IDMyMHB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiAtMzI1cHg7XHJcbiAgbGlzdC1zdHlsZTogbm9uZTtcclxufVxyXG5cclxuLnNsaWRlciAucXVvdGUge1xyXG4gIGZvbnQtc2l6ZTogNDBweDtcclxuICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbn1cclxuXHJcbi5zbGlkZXIgLnNvdXJjZSB7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcblxyXG4uc2xpZGVyIGxpLmFuaW0xIHtcclxuICBhbmltYXRpb246IGN5Y2xlIDE1cyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuXHJcbi5zbGlkZXIgbGkuYW5pbTIge1xyXG4gIGFuaW1hdGlvbjogY3ljbGUyIDE1cyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuXHJcbi5zbGlkZXIgbGkuYW5pbTMge1xyXG4gIGFuaW1hdGlvbjogY3ljbGUzIDE1cyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuXHJcbi5zbGlkZXIgbGkuYW5pbTQge1xyXG4gIGFuaW1hdGlvbjogY3ljbGU0IDE1cyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuXHJcbi5zbGlkZXIgbGkuYW5pbTUge1xyXG4gIGFuaW1hdGlvbjogY3ljbGU1IDE1cyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuXHJcbi5zbGlkZXIgbGkuYW5pbTYge1xyXG4gIGFuaW1hdGlvbjogY3ljbGU2IDE1cyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuXHJcbi5zbGlkZXIgbGkuYW5pbTcge1xyXG4gIGFuaW1hdGlvbjogY3ljbGU3IDE1cyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuXHJcbi5zbGlkZXI6aG92ZXIgbGkge1xyXG4gIGFuaW1hdGlvbi1wbGF5LXN0YXRlOiBwYXVzZWQ7XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgY3ljbGUge1xyXG4gIDAlIHtcclxuICAgIGxlZnQ6IDBweDtcclxuICB9XHJcbiAgNCUge1xyXG4gICAgbGVmdDogMHB4O1xyXG4gIH1cclxuICAxNiUge1xyXG4gICAgbGVmdDogMHB4O1xyXG4gICAgb3BhY2l0eTogMTtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgfVxyXG4gIDIwJSB7XHJcbiAgICBsZWZ0OiAzMjVweDtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gIH1cclxuICAyMSUge1xyXG4gICAgbGVmdDogLTMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIHotaW5kZXg6IC0xO1xyXG4gIH1cclxuICA1MCUge1xyXG4gICAgbGVmdDogLTMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIHotaW5kZXg6IC0xO1xyXG4gIH1cclxuICA5MiUge1xyXG4gICAgbGVmdDogLTMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgfVxyXG4gIDk2JSB7XHJcbiAgICBsZWZ0OiAtMzI1cHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gIH1cclxuICAxMDAlIHtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGN5Y2xlMiB7XHJcbiAgMCUge1xyXG4gICAgbGVmdDogLTMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICB9XHJcbiAgMTYlIHtcclxuICAgIGxlZnQ6IC0zMjVweDtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgfVxyXG4gIDIwJSB7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxuICAyNCUge1xyXG4gICAgbGVmdDogMHB4O1xyXG4gICAgb3BhY2l0eTogMTtcclxuICB9XHJcbiAgMzYlIHtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gIH1cclxuICA0MCUge1xyXG4gICAgbGVmdDogMzI1cHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgei1pbmRleDogMDtcclxuICB9XHJcbiAgNDElIHtcclxuICAgIGxlZnQ6IC0zMjVweDtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICB6LWluZGV4OiAtMTtcclxuICB9XHJcbiAgMTAwJSB7XHJcbiAgICBsZWZ0OiAtMzI1cHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgei1pbmRleDogLTE7XHJcbiAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGN5Y2xlMyB7XHJcbiAgMCUge1xyXG4gICAgbGVmdDogLTMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICB9XHJcbiAgMzYlIHtcclxuICAgIGxlZnQ6IC0zMjVweDtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgfVxyXG4gIDQwJSB7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxuICA0NCUge1xyXG4gICAgbGVmdDogMHB4O1xyXG4gICAgb3BhY2l0eTogMTtcclxuICB9XHJcbiAgNTYlIHtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gIH1cclxuICA2MCUge1xyXG4gICAgbGVmdDogMzI1cHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgei1pbmRleDogMDtcclxuICB9XHJcbiAgNjElIHtcclxuICAgIGxlZnQ6IC0zMjVweDtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICB6LWluZGV4OiAtMTtcclxuICB9XHJcbiAgMTAwJSB7XHJcbiAgICBsZWZ0OiAtMzI1cHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgei1pbmRleDogLTE7XHJcbiAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGN5Y2xlNCB7XHJcbiAgMCUge1xyXG4gICAgbGVmdDogLTMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICB9XHJcbiAgNTYlIHtcclxuICAgIGxlZnQ6IC0zMjVweDtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgfVxyXG4gIDYwJSB7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxuICA2NCUge1xyXG4gICAgbGVmdDogMHB4O1xyXG4gICAgb3BhY2l0eTogMTtcclxuICB9XHJcbiAgNzYlIHtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gIH1cclxuICA4MCUge1xyXG4gICAgbGVmdDogMzI1cHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgei1pbmRleDogMDtcclxuICB9XHJcbiAgODElIHtcclxuICAgIGxlZnQ6IC0zMjVweDtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICB6LWluZGV4OiAtMTtcclxuICB9XHJcbiAgMTAwJSB7XHJcbiAgICBsZWZ0OiAtMzI1cHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgei1pbmRleDogLTE7XHJcbiAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGN5Y2xlNSB7XHJcbiAgMCUge1xyXG4gICAgbGVmdDogLTMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICB9XHJcbiAgNzYlIHtcclxuICAgIGxlZnQ6IC0zMjVweDtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgfVxyXG4gIDgwJSB7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxuICA4NCUge1xyXG4gICAgbGVmdDogMHB4O1xyXG4gICAgb3BhY2l0eTogMTtcclxuICB9XHJcbiAgOTYlIHtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gIH1cclxuICAxMDAlIHtcclxuICAgIGxlZnQ6IDMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGN5Y2xlNiB7XHJcbiAgMCUge1xyXG4gICAgbGVmdDogLTMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICB9XHJcbiAgNzYlIHtcclxuICAgIGxlZnQ6IC0zMjVweDtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgfVxyXG4gIDgwJSB7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxuICA4NCUge1xyXG4gICAgbGVmdDogMHB4O1xyXG4gICAgb3BhY2l0eTogMTtcclxuICB9XHJcbiAgOTYlIHtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gIH1cclxuICAxMDAlIHtcclxuICAgIGxlZnQ6IDMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgfVxyXG59XHJcbkBrZXlmcmFtZXMgY3ljbGU3IHtcclxuICAwJSB7XHJcbiAgICBsZWZ0OiAtMzI1cHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gIH1cclxuICA3NiUge1xyXG4gICAgbGVmdDogLTMyNXB4O1xyXG4gICAgb3BhY2l0eTogMDtcclxuICB9XHJcbiAgODAlIHtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgfVxyXG4gIDg0JSB7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxuICA5NiUge1xyXG4gICAgbGVmdDogMHB4O1xyXG4gICAgb3BhY2l0eTogMTtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgfVxyXG4gIDEwMCUge1xyXG4gICAgbGVmdDogMzI1cHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgei1pbmRleDogMDtcclxuICB9XHJcbn1cclxuXHJcbi8qICoqKioqKioqKioqKioqKioqKioqKioqKipcclxuICAgICAgRW5kIENhcmQgQ2Fyb3VzZWxcclxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKiAqL1xyXG5cclxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gICAgICBDYXJkIFN0eWxlc1xyXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqICovXHJcbi5jYXJkIHtcclxuICBmbGV4OiAxO1xyXG4gIGZsZXgtYmFzaXM6IDMwMHB4O1xyXG4gIGZsZXgtZ3JvdzogMDtcclxuICBoZWlnaHQ6IDMwMHB4O1xyXG4gIHdpZHRoOiAyMDBweDtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIGJvcmRlcjogMnB4IHNvbGlkICNmZmY7XHJcbiAgYm94LXNoYWRvdzogMHB4IDRweCA3cHggcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGN1YmljLWJlemllcigwLjgsIDAuNSwgMC4yLCAxLjQpO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcbi5jYXJkIGltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGN1YmljLWJlemllcigwLjgsIDAuNSwgMC4yLCAxLjQpO1xyXG59XHJcbi5kZXNjcmlwdGlvbnMge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDBweDtcclxuICBsZWZ0OiAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjdzIGVhc2UtaW4tb3V0O1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBjbGlwLXBhdGg6IGNpcmNsZSgwJSBhdCAxMDAlIDEwMCUpO1xyXG59XHJcbi5jYXJkOmhvdmVyIC5kZXNjcmlwdGlvbnMge1xyXG4gIGxlZnQ6IDBweDtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC43cyBlYXNlLWluLW91dDtcclxuICBjbGlwLXBhdGg6IGNpcmNsZSg3NSUpO1xyXG59XHJcbi5jYXJkOmhvdmVyIHtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBjdWJpYy1iZXppZXIoMC44LCAwLjUsIDAuMiwgMS40KTtcclxuICBib3gtc2hhZG93OiAwcHggMnB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMyk7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgwLjk3KTtcclxufVxyXG4uY2FyZDpob3ZlciBpbWcge1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGN1YmljLWJlemllcigwLjgsIDAuNSwgMC4yLCAxLjQpO1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMS42KSByb3RhdGUoMjBkZWcpO1xyXG4gIGZpbHRlcjogYmx1cigzcHgpO1xyXG59XHJcbi5jYXJkIGg1IHtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgdGV4dC1zaGFkb3c6IDAgMCA1cHggYmxhY2s7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcclxuICBtYXJnaW46IDBweDtcclxuICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICBoZWlnaHQ6IDQ1cHg7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuLmNhcmQgcCB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHRleHQtc2hhZG93OiAwIDAgNXB4IGJsYWNrO1xyXG4gIGxpbmUtaGVpZ2h0OiAyNHB4O1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG4uY2FyZC10ZXh0IHtcclxuICBoZWlnaHQ6IDQ3cHg7XHJcbn1cclxuLmNhcmQ6aG92ZXIgLmRlc2Mge1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoNTQsIDU0LCA1NCwgMC4zNTYpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHggMCAwIDEwcHg7XHJcbn1cclxuLmRlc2Mge1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgaGVpZ2h0OiAxNDdweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgcGFkZGluZy1yaWdodDogNDBweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIG1hcmdpbi10b3A6IC0xMnB4O1xyXG4gIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xyXG4gIG1peC1ibGVuZC1tb2RlOiBkaWZmZXJlbmNlO1xyXG59XHJcblxyXG4uY2FyZCBidXR0b24ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDE1cHg7XHJcbiAgd2lkdGg6IDcwcHg7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBib3JkZXItc3R5bGU6IG5vbmU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA0YTMwNDtcclxuICBjb2xvcjogI2ZmZjtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICBib3gtc2hhZG93OiAwcHggMnB4IDNweCByZ2JhKDAsIDAsIDAsIDAuNCk7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZS1pbi1vdXQ7XHJcbn1cclxuLndpc2gge1xyXG4gIHJpZ2h0OiAxMHB4O1xyXG59XHJcbi5jYXJ0IHtcclxuICBsZWZ0OiAxMHB4O1xyXG59XHJcblxyXG4uY2FyZCBidXR0b246aG92ZXIge1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMC45NSkgdHJhbnNsYXRlWCgtNXB4KTtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuLmhpZGUge1xyXG4gIG9wYWNpdHk6IDA7XHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](StaffFavesComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-staff-faves',
                templateUrl: './staff-faves.component.html',
                styleUrls: ['./staff-faves.component.css']
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }, { type: src_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }]; }, null); })();


/***/ }),

/***/ "kWWo":
/*!********************************************!*\
  !*** ./src/app/navbar/navbar.component.ts ***!
  \********************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/auth.service */ "ATD4");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");





function NavbarComponent_ng_container_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "li", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Profile");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "li", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Log Out");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
} }
function NavbarComponent_ng_template_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Log In");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
class NavbarComponent {
    constructor(router, auth) {
        this.router = router;
        this.auth = auth;
        this.isLoggedIn = false;
        this.router.onSameUrlNavigation = 'reload';
        // this.auth.isLoggedIn = false;
    }
    ngOnInit() {
        if (localStorage.getItem('user') != null) {
            this.auth.isLoggedIn = true;
            // this.isLoggedIn = true;
        }
        else {
            this.auth.isLoggedIn = false;
            // this.isLoggedIn = false;
        }
    }
    checkUser() {
    }
}
NavbarComponent.ɵfac = function NavbarComponent_Factory(t) { return new (t || NavbarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"])); };
NavbarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NavbarComponent, selectors: [["app-navbar"]], decls: 20, vars: 2, consts: [[1, "navbar", "navbar-expand-lg", "navbar-secondary", "bg-light"], [1, "container-fluid"], ["routerLink", "/home", 1, "navbar-brand"], ["type", "button", "data-bs-toggle", "collapse", "data-bs-target", "#navbarSupportedContent", "aria-controls", "navbarSupportedContent", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler"], [1, "navbar-toggler-icon"], ["id", "navbarNavDropdown", 1, "collapse", "navbar-collapse"], [1, "links", "navbar-nav", "me-auto", "mb-2", "mb-lg-0"], [1, "nav-item"], ["routerLink", "/cart", 1, "nav-link"], ["routerLink", "/reserved", 1, "nav-link"], ["routerLink", "/wishlist", 1, "nav-link"], [4, "ngIf", "ngIfElse"], ["elseTemplate", ""], ["routerLink", "/profile", 1, "nav-link"], ["routerLink", "/log-out", 1, "nav-link"], ["routerLink", "/login", 1, "nav-link"]], template: function NavbarComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "nav", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Public Library of Mandalore");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "span", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Cart");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Reserved");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Wishlist");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, NavbarComponent_ng_container_17_Template, 7, 0, "ng-container", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, NavbarComponent_ng_template_18_Template, 3, 0, "ng-template", null, 12, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.auth.isLoggedIn)("ngIfElse", _r1);
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"]], styles: [".links[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  right: 20px;\r\n}\r\na[_ngcontent-%COMP%] {\r\n  color: #04a304;\r\n}\r\n.nav-item[_ngcontent-%COMP%] {\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5hdmJhci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCO0VBQ2xCLFdBQVc7QUFDYjtBQUNBO0VBQ0UsY0FBYztBQUNoQjtBQUNBO0FBQ0EiLCJmaWxlIjoibmF2YmFyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGlua3Mge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICByaWdodDogMjBweDtcclxufVxyXG5hIHtcclxuICBjb2xvcjogIzA0YTMwNDtcclxufVxyXG4ubmF2LWl0ZW0ge1xyXG59XHJcbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NavbarComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-navbar',
                templateUrl: './navbar.component.html',
                styleUrls: ['./navbar.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }, { type: src_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }]; }, null); })();


/***/ }),

/***/ "n1B2":
/*!********************************************!*\
  !*** ./src/app/logout/logout.component.ts ***!
  \********************************************/
/*! exports provided: LogoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutComponent", function() { return LogoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/auth.service */ "ATD4");




class LogoutComponent {
    constructor(router, auth) {
        this.router = router;
        this.auth = auth;
    }
    ngOnInit() {
        localStorage.removeItem('user');
        this.auth.signOut();
        setTimeout(() => {
            this.router.navigate(['login']);
        }, 2500);
    }
}
LogoutComponent.ɵfac = function LogoutComponent_Factory(t) { return new (t || LogoutComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"])); };
LogoutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: LogoutComponent, selectors: [["app-logout"]], decls: 3, vars: 0, consts: [[1, "container", "center", "out"]], template: function LogoutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "You have been logged out");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".out[_ngcontent-%COMP%] {\r\n  border: 1px solid black;\r\n  border-radius: 20px;\r\n  width: -webkit-fit-content;\r\n  width: -moz-fit-content;\r\n  width: fit-content;\r\n  color: #04a304;\r\n  padding: 20px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ291dC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQiwwQkFBa0I7RUFBbEIsdUJBQWtCO0VBQWxCLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsYUFBYTtBQUNmIiwiZmlsZSI6ImxvZ291dC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm91dCB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICB3aWR0aDogZml0LWNvbnRlbnQ7XHJcbiAgY29sb3I6ICMwNGEzMDQ7XHJcbiAgcGFkZGluZzogMjBweDtcclxufVxyXG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LogoutComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-logout',
                templateUrl: './logout.component.html',
                styleUrls: ['./logout.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }, { type: src_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }]; }, null); })();


/***/ }),

/***/ "rd1V":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var bcryptjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bcryptjs */ "J5zx");
/* harmony import */ var bcryptjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bcryptjs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var src_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/api.service */ "RcPr");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "ofXK");







function SignupComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function SignupComponent_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Sign Up");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "form", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function SignupComponent_ng_template_1_Template_form_ngSubmit_3_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.signUpSubmit(ctx_r3.signUpForm); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "label", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "label", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "label", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "input", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "label", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "input", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "label", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "label", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Submit");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r2.signUpForm);
} }
class SignupComponent {
    constructor(apiService, formBuilder, router) {
        this.apiService = apiService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.salt = '';
        this.isLoading = false;
        this.signUpForm = this.formBuilder.group({
            firstName: '',
            lastName: '',
            username: '',
            passHash: '',
            email: '',
            phone: '',
        });
    }
    ngOnInit() {
        this.salt = bcryptjs__WEBPACK_IMPORTED_MODULE_1__["genSaltSync"](16);
    }
    signUpSubmit(form) {
        this.isLoading = true;
        console.log(form.value.username);
        form.value.passHash = bcryptjs__WEBPACK_IMPORTED_MODULE_1__["hashSync"](form.value.passHash, this.salt);
        // logic to validate username
        this.apiService.signUp(form.value).subscribe(res => {
            console.log(res);
            alert("Account Created Successfully. Please Login");
            this.isLoading = false;
            this.router.navigate(['login']);
        }, (error) => {
            this.isLoading = false;
            console.log("Duplicate Username");
            alert("Username is already taken");
        });
    }
}
SignupComponent.ɵfac = function SignupComponent_Factory(t) { return new (t || SignupComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"])); };
SignupComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: SignupComponent, selectors: [["app-signup"]], decls: 3, vars: 2, consts: [["class", "container center", 4, "ngIf", "ngIfElse"], ["elseTemplate", ""], [1, "container", "center"], [1, "col-12", "text-center"], ["src", "../../assets/pageTurner.gif", "alt", "Searching...", 1, "mt-4"], [1, "container", "center", "sign-up"], [1, "title"], ["ngNativeValidate", "", 3, "formGroup", "ngSubmit"], [1, "form-group"], ["for", "fname"], ["type", "text", "formControlName", "firstName", "name", "fname", "placeholder", "First Name", "required", "", "minlength", "3"], ["for", "lname"], ["type", "text", "formControlName", "lastName", "placeholder", "Last Name", "required", "", "minlength", "4"], ["for", "username"], ["type", "text", "formControlName", "username", "placeholder", "Username", "required", "", "minlength", "6"], ["for", "password"], ["type", "password", "formControlName", "passHash", "placeholder", "Password", "required", "", "minlength", "6"], ["for", "email"], ["type", "email", "formControlName", "email", "placeholder", "Email", "required", "", "minlength", "6"], ["for", "phone"], ["type", "tel", "formControlName", "phone", "placeholder", "Phone", "required", "", "pattern", "[(][0-9]{3}[)][0-9]{3}-[0-9]{4}", "title", "(123)456-7890"], ["type", "submit", "id", "submit", 1, "mt-4", "btn", "btn-success"]], template: function SignupComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, SignupComponent_div_0_Template, 3, 0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SignupComponent_ng_template_1_Template, 24, 1, "ng-template", null, 1, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
    } if (rf & 2) {
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLoading)("ngIfElse", _r1);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["MinLengthValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["PatternValidator"]], styles: [".sign-up[_ngcontent-%COMP%] {\r\n  border: 1px solid black;\r\n  padding: 10px -30px;\r\n  width: 300px;\r\n  border-radius: 20px;\r\n}\r\n\r\nform[_ngcontent-%COMP%] {\r\n  padding: 0;\r\n  width: 200px;\r\n  margin: 30px auto;\r\n}\r\n\r\n.title[_ngcontent-%COMP%] {\r\n  color: #04a304;\r\n  margin-top: 20px;\r\n  font-size: 20px;\r\n  margin-bottom: -40px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNpZ251cC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixvQkFBb0I7QUFDdEIiLCJmaWxlIjoic2lnbnVwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2lnbi11cCB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XHJcbiAgcGFkZGluZzogMTBweCAtMzBweDtcclxuICB3aWR0aDogMzAwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxufVxyXG5cclxuZm9ybSB7XHJcbiAgcGFkZGluZzogMDtcclxuICB3aWR0aDogMjAwcHg7XHJcbiAgbWFyZ2luOiAzMHB4IGF1dG87XHJcbn1cclxuXHJcbi50aXRsZSB7XHJcbiAgY29sb3I6ICMwNGEzMDQ7XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogLTQwcHg7XHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SignupComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-signup',
                templateUrl: './signup.component.html',
                styleUrls: ['./signup.component.css']
            }]
    }], function () { return [{ type: src_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }, { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }]; }, null); })();


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _books_books_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./books/books.component */ "PRiU");
/* harmony import */ var _cart_cart_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cart/cart.component */ "c2A7");
/* harmony import */ var _editprofile_editprofile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./editprofile/editprofile.component */ "aEgf");
/* harmony import */ var _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./homepage/homepage.component */ "Oh3b");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login/login.component */ "vtpD");
/* harmony import */ var _logout_logout_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./logout/logout.component */ "n1B2");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./profile/profile.component */ "W6KJ");
/* harmony import */ var _reserved_reserved_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./reserved/reserved.component */ "7/yn");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./signup/signup.component */ "rd1V");
/* harmony import */ var _wishlist_wishlist_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./wishlist/wishlist.component */ "x7zu");














const routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_5__["HomepageComponent"] },
    { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_8__["ProfileComponent"] },
    { path: 'edit-profile', component: _editprofile_editprofile_component__WEBPACK_IMPORTED_MODULE_4__["EditprofileComponent"] },
    { path: 'cart', component: _cart_cart_component__WEBPACK_IMPORTED_MODULE_3__["CartComponent"] },
    { path: 'signup', component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_10__["SignupComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"] },
    { path: 'books', component: _books_books_component__WEBPACK_IMPORTED_MODULE_2__["BooksComponent"] },
    { path: 'reserved', component: _reserved_reserved_component__WEBPACK_IMPORTED_MODULE_9__["ReservedComponent"] },
    { path: 'wishlist', component: _wishlist_wishlist_component__WEBPACK_IMPORTED_MODULE_11__["WishlistComponent"] },
    { path: 'log-out', component: _logout_logout_component__WEBPACK_IMPORTED_MODULE_7__["LogoutComponent"] }
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "vtpD":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_models_user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/models/user */ "Oj1a");
/* harmony import */ var bcryptjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! bcryptjs */ "J5zx");
/* harmony import */ var bcryptjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(bcryptjs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../navbar/navbar.component */ "kWWo");
/* harmony import */ var src_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/api.service */ "RcPr");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/auth.service */ "ATD4");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "3Pt+");











function LoginComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function LoginComponent_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Log In");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "form", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function LoginComponent_ng_template_1_Template_form_ngSubmit_3_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.loginSubmit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "label", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "input", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function LoginComponent_ng_template_1_Template_input_ngModelChange_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.username = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "label", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "input", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function LoginComponent_ng_template_1_Template_input_ngModelChange_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.password = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Submit");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Sign Up");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r2.username);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r2.password);
} }
class LoginComponent {
    constructor(apiService, router, nav, auth) {
        this.apiService = apiService;
        this.router = router;
        this.nav = nav;
        this.auth = auth;
        this.username = '';
        this.password = '';
        this.salt = '';
        this.isLoading = false;
    }
    ngOnInit() { }
    loginSubmit() {
        this.isLoading = true;
        let obj = this.apiService.getUser(this.username).subscribe(res => {
            console.log(res);
            if (bcryptjs__WEBPACK_IMPORTED_MODULE_2__["compareSync"](this.password, res.passHash)) {
                this.user = new src_models_user__WEBPACK_IMPORTED_MODULE_1__["User"](res.id, res.firstName, res.lastName, res.username, res.email, res.phone, res.registrationDate);
                localStorage.setItem("user", JSON.stringify(this.user));
                console.log(this.user);
                this.isLoading = false;
                this.auth.signIn();
                this.router.navigate(['home']);
            }
            else {
                alert("Passwords did not match");
                this.router.navigate(['cart']);
            }
        }, (error) => {
            this.isLoading = false;
            console.log("Invalid Username/Password");
            alert("Invalid Username or Password");
        });
    }
}
LoginComponent.ɵfac = function LoginComponent_Factory(t) { return new (t || LoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"])); };
LoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: LoginComponent, selectors: [["app-login"]], features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]])], decls: 3, vars: 2, consts: [["class", "container center", 4, "ngIf", "ngIfElse"], ["elseTemplate", ""], [1, "container", "center"], [1, "col-12", "text-center"], ["src", "../../assets/pageTurner.gif", "alt", "Searching...", 1, "mt-4"], [1, "container", "center", "landing"], [1, "title"], ["id", "login", 3, "ngSubmit"], [1, "form-group"], ["for", "username"], ["type", "text", "name", "username", "placeholder", "Username", 3, "ngModel", "ngModelChange"], ["for", "password"], ["type", "password", "name", "password", "placeholder", "Password", 3, "ngModel", "ngModelChange"], ["type", "submit", "id", "submit", 1, "mt-4", "btn", "btn-success", "ml-1"], ["routerLink", "/signup", 1, "mt-4", "btn", "btn-secondary", "ml-3"]], template: function LoginComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, LoginComponent_div_0_Template, 3, 0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, LoginComponent_ng_template_1_Template, 14, 2, "ng-template", null, 1, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
    } if (rf & 2) {
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLoading)("ngIfElse", _r1);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgModel"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLinkWithHref"]], styles: [".landing[_ngcontent-%COMP%] {\r\n  border: 1px solid black;\r\n  padding: 10px -30px;\r\n  width: 300px;\r\n  border-radius: 20px;\r\n}\r\n\r\nform[_ngcontent-%COMP%] {\r\n  padding: 0;\r\n  width: 200px;\r\n  margin: 30px auto;\r\n}\r\n\r\n#submit[_ngcontent-%COMP%] {\r\n  margin-right: 10px;\r\n}\r\n\r\n.title[_ngcontent-%COMP%] {\r\n  color: #04a304;\r\n  margin-top: 20px;\r\n  font-size: 20px;\r\n  margin-bottom: -40px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQjs7QUFDQTtFQUNFLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLG9CQUFvQjtBQUN0QiIsImZpbGUiOiJsb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxhbmRpbmcge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xyXG4gIHBhZGRpbmc6IDEwcHggLTMwcHg7XHJcbiAgd2lkdGg6IDMwMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbn1cclxuXHJcbmZvcm0ge1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG4gIG1hcmdpbjogMzBweCBhdXRvO1xyXG59XHJcblxyXG4jc3VibWl0IHtcclxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuLnRpdGxlIHtcclxuICBjb2xvcjogIzA0YTMwNDtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBtYXJnaW4tYm90dG9tOiAtNDBweDtcclxufVxyXG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LoginComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                providers: [_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]],
                selector: 'app-login',
                templateUrl: './login.component.html',
                styleUrls: ['./login.component.css']
            }]
    }], function () { return [{ type: src_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }, { type: _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"] }, { type: src_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] }]; }, null); })();


/***/ }),

/***/ "x7zu":
/*!************************************************!*\
  !*** ./src/app/wishlist/wishlist.component.ts ***!
  \************************************************/
/*! exports provided: WishlistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WishlistComponent", function() { return WishlistComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/api.service */ "RcPr");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");





function WishlistComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function WishlistComponent_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "No books were found, add some books to your Wishlist!");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
} }
function WishlistComponent_ng_template_4_div_3_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WishlistComponent_ng_template_4_div_3_div_1_Template_button_click_13_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r7.addToCart($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Cart");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WishlistComponent_ng_template_4_div_3_div_1_Template_button_click_15_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r9.deleteFromWishlist($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Remove");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const book_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", book_r5.book.image, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r5.book.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r5.book.author);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", book_r5.book.description, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](book_r5.book.isbn);
} }
function WishlistComponent_ng_template_4_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, WishlistComponent_ng_template_4_div_3_div_1_Template, 17, 5, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const book_r5 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", book_r5.book);
} }
function WishlistComponent_ng_template_4_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WishlistComponent_ng_template_4_Template_button_click_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r11.emptyWishlist($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Empty Wishlist");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, WishlistComponent_ng_template_4_div_3_Template, 2, 1, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.books);
} }
class WishlistComponent {
    constructor(apiService, router) {
        this.apiService = apiService;
        this.router = router;
        this.isSearching = false;
        this.books = [];
        this.fname = '';
        if (localStorage['user']) {
            this.user = JSON.parse(localStorage['user']);
        }
    }
    ngOnInit() {
        if (localStorage['user']) {
            this.user = JSON.parse(localStorage['user']);
        }
        else {
            this.router.navigate(['login']);
        }
        this.fname = this.user.fname;
        this.isSearching = true;
        this.apiService.getBooksForWishlist(this.user.id).subscribe(res => {
            console.log(res);
            this.isSearching = false;
            this.books = res;
        }, err => {
            this.isSearching = false;
            console.log(err);
        });
    }
    addToCart(e) {
        console.log(e);
        let image = e.path[2].children[0].currentSrc;
        let isbn = e.path[1].children[3].innerText;
        let author = e.path[2].childNodes[1].childNodes[1].innerText;
        let title = e.path[2].childNodes[1].childNodes[0].innerText;
        let desc = e.path[1].childNodes[2].innerText;
        let book = { "title": title, "author": author, "image": image, "isbn": isbn, "description": desc };
        console.log(book);
        this.apiService.addBookToCart(this.user.id, book).subscribe(res => {
            console.log('Book added to cart');
            alert('Book added to cart');
            this.apiService.deleteFromWishlist(isbn, this.user.id).subscribe(res => {
                console.log('Book added to cart');
                alert('Book added to cart');
                let index;
                for (var i = 0; i < this.books.length; i++) {
                    if (this.books[i].book.isbn == isbn) {
                        index = i;
                    }
                }
                this.books.splice(index, 1);
            }, error => {
                console.log(error.error);
                alert(error.error);
            });
        }, error => {
            console.log(error.error);
            alert(error.error);
        });
    }
    emptyWishlist(e) {
        this.apiService.deleteAllFromWishlist(this.user.id).subscribe(res => {
            console.log('All books removed from wishlist');
            alert('All books removed from wishlist');
            this.books = [];
        }, error => {
            console.log(error.error);
            alert(error.error);
        });
    }
    deleteFromWishlist(e) {
        console.log(e);
        let isbn = e.path[1].children[3].innerText;
        this.apiService.deleteFromWishlist(isbn, this.user.id).subscribe(res => {
            console.log('Book removed from wishlist');
            alert('Book removed from wishlist');
            let index;
            for (var i = 0; i < this.books.length; i++) {
                if (this.books[i].book.isbn == isbn) {
                    index = i;
                }
            }
            this.books.splice(index, 1);
        }, error => {
            console.log(error.error);
            alert(error.error);
        });
    }
}
WishlistComponent.ɵfac = function WishlistComponent_Factory(t) { return new (t || WishlistComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"])); };
WishlistComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: WishlistComponent, selectors: [["app-wishlist"]], decls: 6, vars: 4, consts: [["class", "row", 4, "ngIf"], [4, "ngIf", "ngIfElse"], ["elseTemplate", ""], [1, "row"], [1, "col-12", "text-center"], ["src", "../../assets/pageTurner.gif", "alt", "Searching...", 1, "mt-4"], ["role", "alert", 1, "alert", "alert-danger"], [1, "btn", "btn-success", "basket", 3, "click"], ["class", "wrapper", 4, "ngFor", "ngForOf"], [1, "wrapper"], [4, "ngIf"], [1, ""], ["contentEditable", "", "role", "textbox", "aria-multiline", "true", 1, "card"], [3, "src"], [1, "descriptions"], [1, "card-text"], [1, "desc"], [1, "hide"], [1, "btn", "cart", 3, "click"], [1, "btn", "wish", 3, "click"]], template: function WishlistComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, WishlistComponent_div_2_Template, 3, 0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, WishlistComponent_ng_container_3_Template, 5, 0, "ng-container", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, WishlistComponent_ng_template_4_Template, 4, 1, "ng-template", null, 2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
    } if (rf & 2) {
        const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.fname, "'s Wishlist");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isSearching);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.books.length == 0)("ngIfElse", _r2);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"]], styles: ["body[_ngcontent-%COMP%] {\r\n  \r\n  padding: 0px;\r\n  margin: 0px;\r\n  width: 100%;\r\n  height: 100vh;\r\n  font-family: \"Lato\";\r\n}\r\n.row[_ngcontent-%COMP%] {\r\n  justify-content: center;\r\n}\r\n.wrapper[_ngcontent-%COMP%] {\r\n  \r\n  max-width: 300px;\r\n  width: 100%;\r\n  height: auto;\r\n  display: flex;\r\n  flex-wrap: wrap;\r\n  justify-content: center;\r\n  margin: 10px;\r\n}\r\n.card[_ngcontent-%COMP%] {\r\n  flex: 1;\r\n  flex-basis: 300px;\r\n  flex-grow: 0;\r\n  height: 440px;\r\n  width: 300px;\r\n  background: #fff;\r\n  border: 2px solid #fff;\r\n  box-shadow: 0px 4px 7px rgba(0, 0, 0, 0.5);\r\n  cursor: pointer;\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  overflow: hidden;\r\n  position: relative;\r\n}\r\n.card[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 100%;\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n}\r\n.descriptions[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  top: 0px;\r\n  left: 0px;\r\n  width: 100%;\r\n  height: 100%;\r\n  transition: all 0.7s ease-in-out;\r\n  padding: 20px;\r\n  box-sizing: border-box;\r\n  -webkit-clip-path: circle(0% at 100% 100%);\r\n          clip-path: circle(0% at 100% 100%);\r\n}\r\n.card[_ngcontent-%COMP%]:hover   .descriptions[_ngcontent-%COMP%] {\r\n  left: 0px;\r\n  transition: all 0.7s ease-in-out;\r\n  -webkit-clip-path: circle(75%);\r\n          clip-path: circle(75%);\r\n}\r\n.card[_ngcontent-%COMP%]:hover {\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.3);\r\n  transform: scale(0.97);\r\n}\r\n.card[_ngcontent-%COMP%]:hover   img[_ngcontent-%COMP%] {\r\n  transition: all 0.5s cubic-bezier(0.8, 0.5, 0.2, 1.4);\r\n  transform: scale(1.6) rotate(20deg);\r\n  filter: blur(3px);\r\n}\r\n.card[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\r\n  color: white;\r\n  text-shadow: 0 0 5px black;\r\n  letter-spacing: 1px;\r\n  margin: 0px;\r\n  height: 68px;\r\n  overflow: hidden;\r\n}\r\n.card[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  color: white;\r\n  text-shadow: 0 0 5px black;\r\n  line-height: 24px;\r\n  height: 55%;\r\n  font-size: 20px;\r\n}\r\n.card[_ngcontent-%COMP%]:hover   .desc[_ngcontent-%COMP%] {\r\n  background: rgba(54, 54, 54, 0.356);\r\n  border-radius: 10px 0 0 10px;\r\n}\r\n.desc[_ngcontent-%COMP%] {\r\n  height: 150px;\r\n  \r\n  width: 100%;\r\n  overflow-y: scroll;\r\n  overflow-x: hidden;\r\n  padding-right: 50px;\r\n  margin-bottom: 40px;\r\n  box-sizing: content-box;\r\n  mix-blend-mode: difference;\r\n}\r\n.card[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  bottom: 15px;\r\n  width: 100px;\r\n  height: 40px;\r\n  cursor: pointer;\r\n  border-style: none;\r\n  background-color: #04a304;\r\n  color: #fff;\r\n  font-size: 18px;\r\n  outline: none;\r\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.4);\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.card[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover {\r\n  transform: scale(0.95) translateX(-5px);\r\n  transition: all 0.5s ease-in-out;\r\n}\r\n.hide[_ngcontent-%COMP%] {\r\n  opacity: 0;\r\n}\r\n.wish-btn[_ngcontent-%COMP%] {\r\n  max-width: 200px;\r\n  display: block;\r\n}\r\n.wish[_ngcontent-%COMP%] {\r\n  right: 30px;\r\n}\r\n.cart[_ngcontent-%COMP%] {\r\n  left: 30px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndpc2hsaXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBK0I7RUFDL0IsWUFBWTtFQUNaLFdBQVc7RUFDWCxXQUFXO0VBQ1gsYUFBYTtFQUNiLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsdUJBQXVCO0FBQ3pCO0FBQ0E7RUFDRSxzQ0FBc0M7RUFDdEMsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtFQUNiLGVBQWU7RUFDZix1QkFBdUI7RUFDdkIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxPQUFPO0VBQ1AsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixzQkFBc0I7RUFDdEIsMENBQTBDO0VBQzFDLGVBQWU7RUFDZixxREFBcUQ7RUFDckQsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixxREFBcUQ7QUFDdkQ7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FBUztFQUNULFdBQVc7RUFDWCxZQUFZO0VBQ1osZ0NBQWdDO0VBQ2hDLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsMENBQWtDO1VBQWxDLGtDQUFrQztBQUNwQztBQUNBO0VBQ0UsU0FBUztFQUNULGdDQUFnQztFQUNoQyw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSxxREFBcUQ7RUFDckQsMENBQTBDO0VBQzFDLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UscURBQXFEO0VBQ3JELG1DQUFtQztFQUNuQyxpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxZQUFZO0VBQ1osZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsbUNBQW1DO0VBQ25DLDRCQUE0QjtBQUM5QjtBQUNBO0VBQ0UsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QiwwQkFBMEI7QUFDNUI7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osWUFBWTtFQUNaLFlBQVk7RUFDWixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixXQUFXO0VBQ1gsZUFBZTtFQUNmLGFBQWE7RUFDYiwwQ0FBMEM7RUFDMUMsZ0NBQWdDO0FBQ2xDO0FBRUE7RUFDRSx1Q0FBdUM7RUFDdkMsZ0NBQWdDO0FBQ2xDO0FBRUE7RUFDRSxVQUFVO0FBQ1o7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixjQUFjO0FBQ2hCO0FBRUE7RUFDRSxXQUFXO0FBQ2I7QUFDQTtFQUNFLFVBQVU7QUFDWiIsImZpbGUiOiJ3aXNobGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYm9keSB7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogIzNkM2QzZDsgKi9cclxuICBwYWRkaW5nOiAwcHg7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDB2aDtcclxuICBmb250LWZhbWlseTogXCJMYXRvXCI7XHJcbn1cclxuLnJvdyB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuLndyYXBwZXIge1xyXG4gIC8qIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpOyAqL1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIG1hcmdpbjogMTBweDtcclxufVxyXG4uY2FyZCB7XHJcbiAgZmxleDogMTtcclxuICBmbGV4LWJhc2lzOiAzMDBweDtcclxuICBmbGV4LWdyb3c6IDA7XHJcbiAgaGVpZ2h0OiA0NDBweDtcclxuICB3aWR0aDogMzAwcHg7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBib3JkZXI6IDJweCBzb2xpZCAjZmZmO1xyXG4gIGJveC1zaGFkb3c6IDBweCA0cHggN3B4IHJnYmEoMCwgMCwgMCwgMC41KTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgY3ViaWMtYmV6aWVyKDAuOCwgMC41LCAwLjIsIDEuNCk7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLmNhcmQgaW1nIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgY3ViaWMtYmV6aWVyKDAuOCwgMC41LCAwLjIsIDEuNCk7XHJcbn1cclxuLmRlc2NyaXB0aW9ucyB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMHB4O1xyXG4gIGxlZnQ6IDBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuN3MgZWFzZS1pbi1vdXQ7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIGNsaXAtcGF0aDogY2lyY2xlKDAlIGF0IDEwMCUgMTAwJSk7XHJcbn1cclxuLmNhcmQ6aG92ZXIgLmRlc2NyaXB0aW9ucyB7XHJcbiAgbGVmdDogMHB4O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjdzIGVhc2UtaW4tb3V0O1xyXG4gIGNsaXAtcGF0aDogY2lyY2xlKDc1JSk7XHJcbn1cclxuLmNhcmQ6aG92ZXIge1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGN1YmljLWJlemllcigwLjgsIDAuNSwgMC4yLCAxLjQpO1xyXG4gIGJveC1zaGFkb3c6IDBweCAycHggM3B4IHJnYmEoMCwgMCwgMCwgMC4zKTtcclxuICB0cmFuc2Zvcm06IHNjYWxlKDAuOTcpO1xyXG59XHJcbi5jYXJkOmhvdmVyIGltZyB7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgY3ViaWMtYmV6aWVyKDAuOCwgMC41LCAwLjIsIDEuNCk7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgxLjYpIHJvdGF0ZSgyMGRlZyk7XHJcbiAgZmlsdGVyOiBibHVyKDNweCk7XHJcbn1cclxuLmNhcmQgaDMge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICB0ZXh0LXNoYWRvdzogMCAwIDVweCBibGFjaztcclxuICBsZXR0ZXItc3BhY2luZzogMXB4O1xyXG4gIG1hcmdpbjogMHB4O1xyXG4gIGhlaWdodDogNjhweDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcbi5jYXJkIHAge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICB0ZXh0LXNoYWRvdzogMCAwIDVweCBibGFjaztcclxuICBsaW5lLWhlaWdodDogMjRweDtcclxuICBoZWlnaHQ6IDU1JTtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuLmNhcmQ6aG92ZXIgLmRlc2Mge1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoNTQsIDU0LCA1NCwgMC4zNTYpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHggMCAwIDEwcHg7XHJcbn1cclxuLmRlc2Mge1xyXG4gIGhlaWdodDogMTUwcHg7XHJcbiAgLyogaGVpZ2h0OiAxMDAlOyAqL1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcclxuICBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgcGFkZGluZy1yaWdodDogNTBweDtcclxuICBtYXJnaW4tYm90dG9tOiA0MHB4O1xyXG4gIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xyXG4gIG1peC1ibGVuZC1tb2RlOiBkaWZmZXJlbmNlO1xyXG59XHJcblxyXG4uY2FyZCBidXR0b24ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDE1cHg7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIGhlaWdodDogNDBweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgYm9yZGVyLXN0eWxlOiBub25lO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwNGEzMDQ7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgYm94LXNoYWRvdzogMHB4IDJweCAzcHggcmdiYSgwLCAwLCAwLCAwLjQpO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG4uY2FyZCBidXR0b246aG92ZXIge1xyXG4gIHRyYW5zZm9ybTogc2NhbGUoMC45NSkgdHJhbnNsYXRlWCgtNXB4KTtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuLmhpZGUge1xyXG4gIG9wYWNpdHk6IDA7XHJcbn1cclxuXHJcbi53aXNoLWJ0biB7XHJcbiAgbWF4LXdpZHRoOiAyMDBweDtcclxuICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLndpc2gge1xyXG4gIHJpZ2h0OiAzMHB4O1xyXG59XHJcbi5jYXJ0IHtcclxuICBsZWZ0OiAzMHB4O1xyXG59XHJcbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WishlistComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-wishlist',
                templateUrl: './wishlist.component.html',
                styleUrls: ['./wishlist.component.css']
            }]
    }], function () { return [{ type: src_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }]; }, null); })();


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "AytR");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map