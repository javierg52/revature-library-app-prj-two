package com.revature.tests.repositories;

import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.entities.CartBook;
import com.revature.mand.project2.entities.ReservedBook;
import com.revature.mand.project2.repositories.CartBookRepository;
import com.revature.mand.project2.repositories.ReservedBookRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
@Transactional
@Rollback
public class ReservedBookRepositoryTest {

    @Autowired
    private ReservedBookRepository reservedBookRepository;

    @Test
    public void save_ReturnsTrue() {
        ReservedBook book = new ReservedBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setReservationId(195);
        book.setPatronId(20);
        assertTrue(reservedBookRepository.save(book));
    }

    @Test
    public void eligibleToCheckout_ReturnsTrue(){
        assertTrue(reservedBookRepository.eligibleToCheckout(-1, "-1"));
    }

    @Test
    public void eligibleToCheckout_ReturnsFalse(){
        ReservedBook book = new ReservedBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setReservationId(195);
        book.setPatronId(20);
        reservedBookRepository.save(book);

        assertFalse(reservedBookRepository.eligibleToCheckout(20, "9781845931193"));
    }

    @Test
    public void getReservedBooks_ReturnsEmpty(){
        List<ReservedBook> reservedBookList = reservedBookRepository.getReservedBooks(-1);
        assertTrue(reservedBookList.isEmpty());
    }
    @Test
    public void getReservedBooks_ReturnsList(){
        ReservedBook book = new ReservedBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setReservationId(195);
        book.setPatronId(20);
        reservedBookRepository.save(book);

        List<ReservedBook> reservedBookList = reservedBookRepository.getReservedBooks(20);
        assertFalse(reservedBookList.isEmpty());
    }

    @Test
    public void getReturnedBooks_ReturnsEmpty(){
        List<ReservedBook> returnedBookList = reservedBookRepository.getReturnedBooks(-1);
        assertTrue(returnedBookList.isEmpty());
    }

    @Test
    public void getReturnedBooks_ReturnsList(){
        ReservedBook book = new ReservedBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setReservationId(195);
        book.setPatronId(20);
        book.setReturnDate(new Date(1608181200000L));
        reservedBookRepository.save(book);

        List<ReservedBook> returnedBookList = reservedBookRepository.getReturnedBooks(20);
        assertFalse(returnedBookList.isEmpty());
    }

    @Test
    public void returnReservedBook_ReturnsNull(){
        assertTrue(reservedBookRepository.returnReservedBook(-1, "-1") == null);
    }

    @Test
    public void returnReservedBooks_ReturnsList(){
        List<ReservedBook> returnedBookList = reservedBookRepository.returnReservedBooks(-1);
        assertTrue(returnedBookList.isEmpty());
    }


}
