package com.revature.tests.repositories;

import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.entities.CartBook;
import com.revature.mand.project2.repositories.CartBookRepository;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
@Transactional
@Rollback
public class CartBookRepositoryTest {

    @Autowired
    private CartBookRepository cartBookRepository;

    @Test
    public void save_ReturnsTrue() {
        CartBook book = new CartBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setCartId(195);
        book.setPatronId(20);
        assertTrue(cartBookRepository.save(book));
    }

    @Test
    public void getCartBookByPatronIdAndIsbn_ReturnsTrue() {
        CartBook book = new CartBook();

        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setCartId(999);
        book.setPatronId(20);
        cartBookRepository.save(book);

        assertNotNull(cartBookRepository.getCartBookByPatronIdAndIsbn(20, "9781845931193"));
    }

    @Test
    public void getCartBookByPatronIdAndIsbn_ReturnsFalse(){
        CartBook cartBook = cartBookRepository.getCartBookByPatronIdAndIsbn(-1, "1");
        assertTrue(cartBook == null);
    }

    @Test
    public void getByUserId_ReturnsTrue() {
        CartBook book = new CartBook();

        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setCartId(999);
        book.setPatronId(20);
        cartBookRepository.save(book);

        assertNotNull(cartBookRepository.getByUserId(20));
    }

    @Test
    public void getByUserId_ReturnsFalse(){
        List<CartBook> cartBookList = cartBookRepository.getByUserId(-1);
        assertTrue(cartBookList.isEmpty());
    }

    @Test
    public void deleteCartBook_ReturnsTrue() {
        CartBook book = new CartBook();

        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setCartId(999);
        book.setPatronId(20);
        cartBookRepository.save(book);

        assertTrue(cartBookRepository.deleteCartBook(20,"9781845931193"));
    }

    @Test
    public void deleteCartBook_ReturnsFalse(){
        assertFalse(cartBookRepository.deleteCartBook(-1,"12"));
    }

    @Test
    public void deleteCartBookAll_ReturnsTrue() {
        CartBook book = new CartBook();

        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setCartId(999);
        book.setPatronId(20);
        cartBookRepository.save(book);

        assertTrue(cartBookRepository.deleteCartBook(20));
    }

    @Test
    public void deleteCartBookAll_ReturnsFalse(){
        assertFalse(cartBookRepository.deleteCartBook(-1));
    }

}
