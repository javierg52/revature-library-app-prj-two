package com.revature.tests.repositories;

import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.entities.ReservedBook;
import com.revature.mand.project2.entities.WishlistBook;
import com.revature.mand.project2.repositories.WishlistBookRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
@Transactional
@Rollback
public class WishlistBookRepositoryTest {

    @Autowired
    private WishlistBookRepository wishlistBookRepository;

    @Test
    public void save_ReturnsTrue() {
        WishlistBook book = new WishlistBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setRank(1);
        book.setPatronId(20);
        assertTrue(wishlistBookRepository.save(book));
    }
    @Test
    public void getWishlistBookByPatronIdAndIsbn_ReturnsList(){
        WishlistBook book = new WishlistBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setRank(1);
        book.setPatronId(20);
        wishlistBookRepository.save(book);

        WishlistBook wishlistBook = wishlistBookRepository.getWishlistBookByPatronIdAndIsbn(20,"9781845931193");
        assertNotNull(wishlistBook);
    }

    @Test
    public void getWishlistBookByPatronIdAndIsbn_ReturnsNull(){
        WishlistBook wishlistBook = wishlistBookRepository.getWishlistBookByPatronIdAndIsbn(-1,"-1");
        assertTrue(wishlistBook == null);
    }

    @Test
    public void getByUserId_ReturnsList(){
        WishlistBook book = new WishlistBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setRank(1);
        book.setPatronId(20);
        wishlistBookRepository.save(book);

        List<WishlistBook> wishlistBookList = wishlistBookRepository.getByUserId(20);
        assertFalse(wishlistBookList.isEmpty());
    }

    @Test
    public void getByUserId_ReturnsEmpty(){
        List<WishlistBook> wishlistBookList = wishlistBookRepository.getByUserId(-1);
        assertTrue(wishlistBookList.isEmpty());
    }
    @Test
    public void deleteWishlistBook_ReturnsTrue(){
        WishlistBook book = new WishlistBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setRank(1);
        book.setPatronId(20);
        wishlistBookRepository.save(book);

        assertTrue(wishlistBookRepository.deleteWishlistBook(20,"9781845931193"));
    }

    @Test
    public void deleteWishlistBook_ReturnsFalse(){
        assertFalse(wishlistBookRepository.deleteWishlistBook(-1,"-1"));
    }

    @Test
    public void deleteWishlistBooks_ReturnsTrue(){
        WishlistBook book = new WishlistBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setRank(1);
        book.setPatronId(20);
        wishlistBookRepository.save(book);

        assertTrue(wishlistBookRepository.deleteWishlistBook(20));
    }

    @Test
    public void deleteWishlistBooks_ReturnsFalse(){
        assertFalse(wishlistBookRepository.deleteWishlistBook(-1));
    }
}
