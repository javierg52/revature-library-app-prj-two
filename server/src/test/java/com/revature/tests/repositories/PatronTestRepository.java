package com.revature.tests.repositories;

import com.revature.mand.project2.entities.Patron;
import com.revature.mand.project2.repositories.PatronRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
@Transactional
@Rollback
public class PatronTestRepository {

    @Autowired
    private PatronRepository patronRepository;

    private Patron patron;


    @Test
    public void getById_ReturnsNotNull() {
        assertNotNull(patronRepository.getById(20));
    }

    @Test
    public void getByUserName_ReturnsNotNull() {
        assertNotNull(patronRepository.getByUserName("iAmError"));
    }

    @Test
    public void getByUserName_ReturnsNull(){
        assertTrue(patronRepository.getByUserName("Notaname12432") == null);
    }

    @Test
    public void save_ReturnsTrue() {
        patron = new Patron();
        //Set Patron Values
        patron.setId(99);
        patron.setFirstName("Rick");
        patron.setLastName("Sanchez");
        patron.setEmail("wubbalubba@dub.dub");
        patron.setUsername("rickC147");
        patron.setPassHash("thispasswordwillnotwork");
        patron.setRegistrationDate(new Date(1608181200000L));

        assertTrue(patronRepository.save(patron));
    }
}
