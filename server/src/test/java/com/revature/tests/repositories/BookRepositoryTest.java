package com.revature.tests.repositories;

import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.repositories.BookRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
@Transactional
@Rollback
public class BookRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @Test
    public void getByIsbn_ReturnsNull(){
        assertEquals(null, bookRepository.getByIsbn("xxxxyyyyyzzzzzz"));
    }

    @Test
    public void getByIsbn_ReturnsBook(){
        Book book = bookRepository.getByIsbn("0553900323");
        assertTrue(book instanceof Book);
    }

}
