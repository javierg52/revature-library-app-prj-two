package com.revature.tests.services;

import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.entities.ReservedBook;
import com.revature.mand.project2.repositories.ReservedBookRepository;
import com.revature.mand.project2.repositories.WishlistBookRepository;
import com.revature.mand.project2.services.DeleteWishlistBookService;
import com.revature.mand.project2.services.ReturnReservedBookService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Date;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class ReturnReservedBookServiceTest {

    @Mock
    private ReservedBookRepository reservedBookRepository;
    private ReturnReservedBookService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.service = new ReturnReservedBookService(reservedBookRepository);
    }

    @Test
    public void returnReservedBook_shouldReturnTrue() {
        ReservedBook book = new ReservedBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setReservationId(195);
        book.setPatronId(20);
        book.setDueDate(new Date(1608181200000L));

        when(reservedBookRepository.returnReservedBook(anyInt(), anyString()))
                .thenReturn(book);
        assertEquals("Your book has been returned",
                service.returnReservedBook(20, ""));
    }
    @Test
    public void returnReservedBook_shouldReturnFalse() {
        when(reservedBookRepository.returnReservedBook(anyInt(), anyString()))
                .thenReturn(null);
        assertEquals("An error occurred processing your request",
                service.returnReservedBook(20, ""));
    }

    @Test
    public void returnReservedBooks_shouldReturnTrue() {
        ReservedBook book = new ReservedBook();
        book.setBook(new Book());
        book.getBook().setIsbn("9781845931193");
        book.setReservationId(195);
        book.setPatronId(20);
        book.setDueDate(new Date(1608181200000L));

        ArrayList<ReservedBook> list = new ArrayList<ReservedBook>();
        list.add(book);
        when(reservedBookRepository.returnReservedBooks(anyInt()))
                .thenReturn(list);
        assertEquals("All books have been returned",
                service.returnReservedBooks(20));
    }
    @Test
    public void returnReservedBooks_shouldReturnFalse() {
        when(reservedBookRepository.returnReservedBooks(anyInt()))
                .thenReturn(null);
        assertEquals("An error occurred processing your request",
                service.returnReservedBooks(20));
    }
}