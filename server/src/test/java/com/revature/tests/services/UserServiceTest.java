package com.revature.tests.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.mand.project2.entities.Patron;
import com.revature.mand.project2.repositories.PatronRepository;
import com.revature.mand.project2.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@Commit
@Transactional
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
public class UserServiceTest {

    private UserService userService;
    //Mock Repository
    @Mock
    private PatronRepository mockRepo;
    @Mock
    private Patron mockPatron;

    ObjectMapper om;

    //Setup methods
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        userService = new UserService(mockRepo);

        om = new ObjectMapper();
        mockPatron = new Patron();
        //Set Patron Values
        mockPatron.setId(20);
        mockPatron.setFirstName("Testy");
        mockPatron.setLastName("Testerson");
        mockPatron.setEmail("test@testerson.com");
        mockPatron.setUsername("iAmError");
        mockPatron.setPassHash("thispasswordwillnotwork");
        mockPatron.setRegistrationDate(new Date(1608181200000L));
    }

    //Test methods

    //getUserByUserName -- Tests
    @Test
    public void getUserByUserName_shouldReturnJSON() {
        when(mockRepo.getByUserName("iAmError"))
                .thenReturn(mockPatron);
        assertEquals(mockPatron, mockRepo.getByUserName("iAmError"));
        userService.getUserByUserName("iAmError");
        verify(mockRepo, times(2)).getByUserName("iAmError");
        verifyNoMoreInteractions(mockRepo);
    }

}
