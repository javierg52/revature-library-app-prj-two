package com.revature.tests.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.entities.ReservedBook;
import com.revature.mand.project2.repositories.ReservedBookRepository;
import com.revature.mand.project2.services.RetrieveReservedBookService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

public class RetrieveReservedBookServiceTest {

    @Mock
    private ReservedBookRepository reservedBookRepository;

    private RetrieveReservedBookService service;
    private ReservedBook book;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service = new RetrieveReservedBookService(reservedBookRepository);

        book = new ReservedBook();

        //Create Mock Book Object
        Book b1 = new Book();
        b1.setIsbn("9781681495057");
        b1.setTitle("Ignatius Bible, 2nd Edition");
        b1.setAuthor("Unknown");
        b1.setImage("http://books.google.com/books/content?id=mU-JCwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api");
        b1.setDescription("The Bible");

        book.setBook(b1);
        book.setPatronId(20);
        book.setReservationId(999);
        book.setReservationDate(new Date(1608181200000L));
    }

    @Test
    public void getReservedBooks_shouldReturnJSON() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        String testJSON = "[{\"reservationId\":999," +
                "\"patronId\":20," +
                "\"book\":{\"isbn\":\"9781681495057\"," +
                "\"title\":\"Ignatius Bible, 2nd Edition\"," +
                "\"author\":\"Unknown\"," +
                "\"image\":\"http://books.google.com/books/content?id=mU-JCwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api\"," +
                "\"description\":\"The Bible\"," +
                "\"copiesCheckedOut\":0}," +
                "\"reservationDate\":\"12/17/2020\"," +
                "\"reservationTerm\":0," +
                "\"dueDate\":null," +
                "\"returnDate\":null," +
                "\"late\":false}]";

        ArrayList<ReservedBook> list = new ArrayList<ReservedBook>();
        list.add(book);
        when(reservedBookRepository.getReservedBooks(anyInt()))
                .thenReturn(list);
        assertEquals(testJSON, service.getReservedBooks(20));
    }
    @Test
    public void getReservedBooks_shouldReturnEmpty() {
        when(reservedBookRepository.getReservedBooks(anyInt()))
                .thenReturn(new ArrayList<>());
        assertEquals("[]", service.getReservedBooks(20));
    }
}