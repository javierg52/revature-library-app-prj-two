package com.revature.tests.services;

import com.revature.mand.project2.repositories.CartBookRepository;
import com.revature.mand.project2.services.DeleteCartBookService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class DeleteCartBookServiceTest {

    @Mock
    private CartBookRepository cartBookRepository;
    private DeleteCartBookService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.service = new DeleteCartBookService(cartBookRepository);
    }

    @Test
    public void deleteCartBook_shouldReturnTrue() {
        when(cartBookRepository.deleteCartBook(anyInt(), anyString()))
                .thenReturn(true);
        assertEquals("Book has been removed from cart.",
                service.deleteCartBook(20, ""));
    }
    @Test
    public void deleteCartBook_shouldReturnFalse() {
        when(cartBookRepository.deleteCartBook(anyInt(), anyString()))
                .thenReturn(false);
        assertEquals("An error occurred processing your request",
                service.deleteCartBook(20, ""));
    }

    @Test
    public void deleteCartBooks_shouldReturnTrue() {
        when(cartBookRepository.deleteCartBook(anyInt()))
                .thenReturn(true);
        assertEquals("All books have been removed from cart.",
                service.deleteCartBook(20));
    }
    @Test
    public void deleteCartBooks_shouldReturnFalse() {
        when(cartBookRepository.deleteCartBook(anyInt()))
                .thenReturn(false);
        assertEquals("An error occurred processing your request",
                service.deleteCartBook(20));
    }
}
