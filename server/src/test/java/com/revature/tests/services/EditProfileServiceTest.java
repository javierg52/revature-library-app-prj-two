package com.revature.tests.services;

import com.revature.mand.project2.entities.Patron;
import com.revature.mand.project2.repositories.PatronRepository;
import com.revature.mand.project2.services.EditProfileService;
import com.revature.mand.project2.services.InstantiatePatronService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class EditProfileServiceTest {

    @Mock
    private PatronRepository patronRepository;
    @Mock
    private InstantiatePatronService instantiatePatronService;

    private EditProfileService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service = new EditProfileService(patronRepository, instantiatePatronService);
    }

    @Test
    public void editProfile_shouldReturnTrue() {
        when(instantiatePatronService.instantiatePatron(anyString()))
                .thenReturn(new Patron());
        when(patronRepository.getById(anyInt()))
                .thenReturn(new Patron());
        assertTrue(service.editProfile("", 20));
    }
    @Test
    public void editProfile_shouldReturnNull() {
        when(instantiatePatronService.instantiatePatron(anyString()))
                .thenReturn(null);
        assertFalse(service.editProfile("", 20));
    }
    @Test
    public void editProfile_shouldReturnFalse() {
        when(instantiatePatronService.instantiatePatron(anyString()))
                .thenReturn(new Patron());
        when(patronRepository.getById(anyInt()))
                .thenReturn(null);
        assertFalse(service.editProfile("", 20));
    }
}
