package com.revature.tests.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.services.InstantiateBookService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class InstantiateBookServiceTest {

    @Mock
    private ObjectMapper om;

    private InstantiateBookService service;
    private Book book;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service = new InstantiateBookService();

        book = new Book();

        //Create Mock Book Object
        book.setIsbn("9781681495057");
        book.setTitle("Ignatius Bible, 2nd Edition");
        book.setAuthor("Unknown");
        book.setImage("http://books.google.com/books/content?id=mU-JCwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api");
        book.setDescription("The Bible");
    }

    @Test
    public void instantiateBook_shouldReturnBook() throws JsonProcessingException {
        ObjectMapper objectmapper = new ObjectMapper();
        String testJSON = objectmapper.writeValueAsString(book);

        when(om.readValue(testJSON, Book.class))
                .thenReturn(new Book());
        assertNotNull(service.instantiateBook(testJSON));
    }
    @Test
    public void instantiateBook_shouldThrowException() throws JsonProcessingException {
        ObjectMapper objectmapper = new ObjectMapper();
        String testJSON = objectmapper.writeValueAsString(book);

        when(om.readValue(testJSON, Book.class))
                .thenThrow(new JsonProcessingException("Error"){})
                .thenReturn(new Book());
        assertNotNull(service.instantiateBook(testJSON));
    }
}