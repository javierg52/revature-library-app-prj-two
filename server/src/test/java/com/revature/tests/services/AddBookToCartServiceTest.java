package com.revature.tests.services;

import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.entities.CartBook;
import com.revature.mand.project2.repositories.BookRepository;
import com.revature.mand.project2.repositories.CartBookRepository;
import com.revature.mand.project2.repositories.ReservedBookRepository;
import com.revature.mand.project2.services.AddBookToCartService;
import com.revature.mand.project2.services.InstantiateBookService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
public class AddBookToCartServiceTest {

    @Mock
    private InstantiateBookService instantiateBookServiceMock;
    @Mock
    private BookRepository bookRepositoryMock;
    @Mock
    private CartBookRepository cartBookRepositoryMock;
    @Mock
    private Book bookMock;
    @Mock
    private CartBook cartBookMock;
    @Mock
    private ReservedBookRepository reservedBookRepositoryMock;

    private AddBookToCartService addBookToCartService;

    //Setup methods
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.addBookToCartService = new AddBookToCartService(cartBookRepositoryMock,
                instantiateBookServiceMock,reservedBookRepositoryMock,bookRepositoryMock);
    }

    @Test
    public void addCartBook_ReturnsInvalid(){
        when(instantiateBookServiceMock.instantiateBook("")).
                thenReturn(null);
        assertEquals("Invalid Book Input", addBookToCartService.addCartBook("",1));
    }

    @Test
    public void addCartBook_ReturnsError(){
        when(instantiateBookServiceMock.instantiateBook("")).
                thenReturn(new Book());
        when(bookRepositoryMock.getByIsbn(anyString())).thenReturn(null);
        when(cartBookRepositoryMock.save(new CartBook())).thenReturn(false);
        assertEquals("There was an error processing your request.", addBookToCartService.addCartBook("",1));
    }

    @Test
    public void addCartBook_ReturnsSuccess(){
        when(instantiateBookServiceMock.instantiateBook("")).
                thenReturn(new Book());
        when(bookRepositoryMock.getByIsbn(anyString())).thenReturn(null);
        when(cartBookRepositoryMock.save(anyObject())).thenReturn(true);
        assertEquals("Successfully added book to cart", addBookToCartService.addCartBook("",1));
    }

    @Test
    public void addCartBook_ReturnsBookAlreadyInCart(){
        when(instantiateBookServiceMock.instantiateBook(anyString())).
                thenReturn(new Book());
        when(bookRepositoryMock.getByIsbn(anyString())).thenReturn(new Book());
        when(cartBookRepositoryMock.getCartBookByPatronIdAndIsbn(anyInt(),anyString()))
                .thenReturn(new CartBook());
        assertEquals("Book is already in cart", addBookToCartService.addCartBook("",1));
    }

    @Test
    public void addCartBook_ReturnsBookUnavailable(){
        when(instantiateBookServiceMock.instantiateBook("")).
                thenReturn(new Book());
        Book book = new Book();
        book.setCopiesCheckedOut(2);
        when(bookRepositoryMock.getByIsbn(anyString())).thenReturn(book);
        when(cartBookRepositoryMock.getCartBookByPatronIdAndIsbn(anyInt(),anyString()))
                .thenReturn(null);
        assertEquals("Book is unavailable. All copies checked out.", addBookToCartService.addCartBook("",1));
    }

    @Test
    public void addCartBook_ReturnsBookCheckedOutPast90(){
        when(instantiateBookServiceMock.instantiateBook("")).
                thenReturn(new Book());
        when(bookRepositoryMock.getByIsbn(anyString())).thenReturn(new Book());
        when(cartBookRepositoryMock.getCartBookByPatronIdAndIsbn(anyInt(),anyString()))
                .thenReturn(null);
        when(bookMock.getCopiesCheckedOut()).thenReturn(1);
        when(reservedBookRepositoryMock.eligibleToCheckout(1,""))
        .thenReturn(false);
        assertEquals("You are not eligible to reserve this book because you have" +
                " reserved it in the past 90 days.", addBookToCartService.addCartBook("",1));
    }

}
