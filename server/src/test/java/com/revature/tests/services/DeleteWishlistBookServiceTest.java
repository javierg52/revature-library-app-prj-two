package com.revature.tests.services;

import com.revature.mand.project2.repositories.WishlistBookRepository;
import com.revature.mand.project2.services.DeleteWishlistBookService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class DeleteWishlistBookServiceTest {

    @Mock
    private WishlistBookRepository wishlistBookRepository;
    private DeleteWishlistBookService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.service = new DeleteWishlistBookService(wishlistBookRepository);
    }

    @Test
    public void deleteCartBook_shouldReturnTrue() {
        when(wishlistBookRepository.deleteWishlistBook(anyInt(), anyString()))
                .thenReturn(true);
        assertEquals("Successfully removed book from your wishlist.",
                service.deleteWishlistBook(20, ""));
    }
    @Test
    public void deleteCartBook_shouldReturnFalse() {
        when(wishlistBookRepository.deleteWishlistBook(anyInt(), anyString()))
                .thenReturn(false);
        assertEquals("An error occurred removing book from your wishlist.",
                service.deleteWishlistBook(20, ""));
    }

    @Test
    public void deleteCartBooks_shouldReturnTrue() {
        when(wishlistBookRepository.deleteWishlistBook(anyInt()))
                .thenReturn(true);
        assertEquals("Successfully removed all books from your wishlist.",
                service.deleteWishlistBook(20));
    }
    @Test
    public void deleteCartBooks_shouldReturnFalse() {
        when(wishlistBookRepository.deleteWishlistBook(anyInt()))
                .thenReturn(false);
        assertEquals("An error occurred removing all books from your wishlist.",
                service.deleteWishlistBook(20));
    }
}