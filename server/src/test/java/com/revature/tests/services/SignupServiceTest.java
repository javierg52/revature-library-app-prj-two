package com.revature.tests.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.mand.project2.entities.Patron;
import com.revature.mand.project2.repositories.PatronRepository;
import com.revature.mand.project2.services.SignupService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@Commit
@Transactional
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
public class SignupServiceTest {

    private SignupService signupService;
    //Mock Repository
    @Mock
    private PatronRepository mockRepo;
    //@Mock
    private Patron mockPatron;
    @Mock
    private ObjectMapper om;

    //Setup methods
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        signupService = new SignupService(mockRepo);

        mockPatron = new Patron();
        //Set Patron Values
        mockPatron.setId(20);
        mockPatron.setFirstName("Testy");
        mockPatron.setLastName("Testerson");
        mockPatron.setEmail("test@testerson.com");
        mockPatron.setUsername("iAmError");
        mockPatron.setPassHash("thispasswordwillnotwork");
        mockPatron.setRegistrationDate(new Date(1608181200000L));
    }

    //Test methods

    //registerUser -- Tests
    @Test
    public void registerUser_shouldReturnTrue() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        //Convert POJO to JSON
        String testJSON = mapper.writeValueAsString(mockPatron);

        when(mockRepo.save(anyObject()))
                .thenReturn(true);
        when(mockRepo.getByUserName(anyString()))
                .thenReturn(null);

        assertEquals(true, mockRepo.save(anyObject()));
        signupService.registerUser(testJSON);
        verify(mockRepo, times(1)).save(mockPatron);
//        verifyNoMoreInteractions(mockRepo);
    }
    @Test
    public void registerUser_shouldReturnFalse() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        //Convert POJO to JSON
        String testJSON = mapper.writeValueAsString(mockPatron);

        when(mockRepo.save(anyObject()))
                .thenReturn(false);
        when(mockRepo.getByUserName(anyString()))
                .thenReturn(null);

        assertEquals(false, mockRepo.save(anyObject()));
        signupService.registerUser(testJSON);
        verify(mockRepo, times(1)).save(mockPatron);
//        verifyNoMoreInteractions(mockRepo);
    }
    @Test
    public void registerUser_shouldReturnNull() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        //Convert POJO to JSON
        String testJSON = mapper.writeValueAsString(mockPatron);

        when(mockRepo.save(anyObject()))
                .thenReturn(false);
        when(mockRepo.getByUserName(anyString()))
                .thenReturn(mockPatron);

        signupService.registerUser(testJSON);
        verify(mockRepo, times(1)).getByUserName(mockPatron.getUsername());
//        verifyNoMoreInteractions(mockRepo);
    }
}
