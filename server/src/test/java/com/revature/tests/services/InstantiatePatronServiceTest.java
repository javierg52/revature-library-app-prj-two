package com.revature.tests.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.entities.Patron;
import com.revature.mand.project2.services.InstantiatePatronService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Date;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class InstantiatePatronServiceTest {

    @Mock
    private ObjectMapper om;

    private InstantiatePatronService service;
    private Patron patron;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service = new InstantiatePatronService();

        patron = new Patron();

        //Create Mock Book Object
        patron.setId(99);
        patron.setFirstName("Rick");
        patron.setLastName("Sanchez");
        patron.setUsername("rickC137");
        patron.setPassHash("thispasswordwillnotwork");
        patron.setEmail("wubbalubba@dub.dub");
        patron.setRegistrationDate(new Date(1608181200000L));
    }

    @Test
    public void instantiatePatron_shouldReturnBook() throws JsonProcessingException {
        ObjectMapper objectmapper = new ObjectMapper();
        String testJSON = objectmapper.writeValueAsString(patron);

        when(om.readValue(testJSON, Patron.class))
                .thenReturn(new Patron());
        assertNotNull(service.instantiatePatron(testJSON));
    }
    @Test
    public void instantiatePatron_shouldThrowException() throws JsonProcessingException {
        ObjectMapper objectmapper = new ObjectMapper();
        String testJSON = objectmapper.writeValueAsString(patron);

        when(om.readValue(testJSON, Patron.class))
                .thenThrow(new JsonProcessingException("Error"){})
                .thenReturn(new Patron());
        assertNotNull(service.instantiatePatron(testJSON));
    }
}