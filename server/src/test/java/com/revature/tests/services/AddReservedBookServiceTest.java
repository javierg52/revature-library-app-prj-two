package com.revature.tests.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.entities.ReservedBook;
import com.revature.mand.project2.repositories.ReservedBookRepository;
import com.revature.mand.project2.services.AddBookToCartService;
import com.revature.mand.project2.services.AddReservedBookService;
import com.revature.mand.project2.services.InstantiateBookService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"classpath:test-application-context.xml"})
public class AddReservedBookServiceTest {

    @Mock
    private ReservedBookRepository reservedBookRepositoryMock;
    @Mock
    private InstantiateBookService instantiateBookServiceMock;

    private AddReservedBookService addReservedBookService;

    private Book[] book;

    //Setup methods
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.addReservedBookService = new AddReservedBookService(
                reservedBookRepositoryMock,instantiateBookServiceMock);

        Book b1 = new Book();
        b1.setTitle("Random Book");
        b1.setImage("https://pyxis.nymag.com/v1/imgs/11f/cc2/28165a08cf31e2f49341c39aee26b6bd01-02-troll-face.rsocial.w1200.jpg");
        b1.setAuthor("Anonymous");
        b1.setIsbn("9781889093321");
        b1.setDescription("LOL");
        b1.setCopiesCheckedOut(0);

        book = new Book[]{b1};

    }
    @Test
    public void addReservedBooks_ReturnsSuccess() throws JsonProcessingException {
        when(instantiateBookServiceMock.instantiateBookArray(anyString()))
                .thenReturn(book);
        when(reservedBookRepositoryMock.save(anyObject()))
                .thenReturn(true);
        assertEquals("Your books have been reserved",
                addReservedBookService.addReservedBooks("",20));
    }
    @Test
    public void addReservedBooks_ReturnsFailure() throws JsonProcessingException {
        when(instantiateBookServiceMock.instantiateBookArray(anyString()))
                .thenReturn(book);
        when(reservedBookRepositoryMock.save(anyObject()))
                .thenReturn(false);
        assertEquals("An error occurred processing your request",
                addReservedBookService.addReservedBooks("",20));
    }

    @Test
    public void addReservedBooks_ReturnsError1(){
        when(instantiateBookServiceMock.instantiateBook(anyString())).
                thenReturn(null);
        assertEquals("An error occurred processing your request",
                addReservedBookService.addReservedBooks("",1));
    }

    @Test
    public void addReservedBooks_ReturnsError2(){
        when(instantiateBookServiceMock.instantiateBook(anyString())).
                thenReturn(anyObject());
        assertEquals("An error occurred processing your request",
                addReservedBookService.addReservedBooks("",1));
    }

}
