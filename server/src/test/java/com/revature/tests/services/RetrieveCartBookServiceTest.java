package com.revature.tests.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.entities.CartBook;
import com.revature.mand.project2.repositories.CartBookRepository;
import com.revature.mand.project2.services.RetrieveCartBookService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

public class RetrieveCartBookServiceTest {

    @Mock
    private CartBookRepository cartBookRepository;

    private RetrieveCartBookService service;
    private CartBook book;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service = new RetrieveCartBookService(cartBookRepository);

        book = new CartBook();

        //Create Mock Book Object
        Book b1 = new Book();
        b1.setIsbn("9781681495057");
        b1.setTitle("Ignatius Bible, 2nd Edition");
        b1.setAuthor("Unknown");
        b1.setImage("http://books.google.com/books/content?id=mU-JCwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api");
        b1.setDescription("The Bible");

        book.setBook(b1);
        book.setPatronId(20);
        book.setCartId(999);
    }

    @Test
    public void getCartBookById_shouldReturnJSON() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        String testJSON = "[{\"cartId\":999," +
                "\"patronId\":20," +
                "\"book\":{\"isbn\":\"9781681495057\"," +
                "\"title\":\"Ignatius Bible, 2nd Edition\"," +
                "\"author\":\"Unknown\"," +
                "\"image\":\"http://books.google.com/books/content?id=mU-JCwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api\"," +
                "\"description\":\"The Bible\"," +
                "\"copiesCheckedOut\":0}," +
                "\"toCheckout\":false}]";

        ArrayList<CartBook> list = new ArrayList<CartBook>();
        list.add(book);
        when(cartBookRepository.getByUserId(anyInt()))
                .thenReturn(list);
        assertEquals(testJSON, service.getCartBookById(20));
    }
    @Test
    public void getCartBookById_shouldReturnEmpty() {
        when(cartBookRepository.getByUserId(anyInt()))
                .thenReturn(new ArrayList<>());
        assertEquals("[]", service.getCartBookById(20));
    }
}