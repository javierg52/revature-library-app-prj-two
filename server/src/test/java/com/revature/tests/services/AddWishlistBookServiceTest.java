package com.revature.tests.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revature.mand.project2.entities.Book;
import com.revature.mand.project2.entities.WishlistBook;
import com.revature.mand.project2.repositories.BookRepository;
import com.revature.mand.project2.repositories.ReservedBookRepository;
import com.revature.mand.project2.repositories.WishlistBookRepository;
import com.revature.mand.project2.services.AddReservedBookService;
import com.revature.mand.project2.services.AddWishlistBookService;
import com.revature.mand.project2.services.InstantiateBookService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AddWishlistBookServiceTest {

    @Mock
    private WishlistBookRepository wishlistBookRepository;
    @Mock
    private BookRepository bookRepository;
    @Mock
    private InstantiateBookService instantiateBookService;

    private AddWishlistBookService service;

    private Book book;

    //Setup methods
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.service = new AddWishlistBookService(
                wishlistBookRepository, instantiateBookService, bookRepository);

        book = new Book();
        book.setTitle("Random Book");
        book.setImage("https://pyxis.nymag.com/v1/imgs/11f/cc2/28165a08cf31e2f49341c39aee26b6bd01-02-troll-face.rsocial.w1200.jpg");
        book.setAuthor("Anonymous");
        book.setIsbn("9781889093321");
        book.setDescription("LOL");
        book.setCopiesCheckedOut(0);

    }
    @Test
    public void addWishlistBook_ReturnsSuccess() throws JsonProcessingException {
        when(instantiateBookService.instantiateBook(anyString()))
                .thenReturn(book);
        when(wishlistBookRepository.save(anyObject()))
                .thenReturn(true);
        assertEquals("Successfully added to Wishlist",
                service.addWishlistBook("",20));
    }
    @Test
    public void addWishlistBook_ReturnsFailure() throws JsonProcessingException {
        when(instantiateBookService.instantiateBook(anyString()))
                .thenReturn(book);
        when(wishlistBookRepository.getWishlistBookByPatronIdAndIsbn(anyInt(), anyString()))
                .thenReturn(new WishlistBook());
        assertEquals("Book is already in wishlist",
                service.addWishlistBook("",20));
    }

    @Test
    public void addWishlistBook_ReturnsError1(){
        when(instantiateBookService.instantiateBook(anyString())).
                thenReturn(null);
        assertEquals("There was an error processing your request.",
                service.addWishlistBook("",20));
    }

    @Test
    public void addWishlistBook_ReturnsError2(){
        when(instantiateBookService.instantiateBook(anyString()))
                .thenReturn(book);
        when(wishlistBookRepository.save(anyObject()))
                .thenReturn(false);

        assertEquals("There was an error processing your request.",
                service.addWishlistBook("",20));
    }
}